﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadSceneAsync : MonoBehaviour
{
    private AsyncOperation asyncLoad;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LoadAsyncScene());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Image image;
    IEnumerator LoadAsyncScene()
    {
        yield return new WaitForSeconds(1);

        asyncLoad = SceneManager.LoadSceneAsync("Demo");
        asyncLoad.allowSceneActivation = false;
        float time = Time.time + 5f;
        while(!asyncLoad.isDone || Time.time <= time)
        {
            if(Time.time > time)
            {
                asyncLoad.allowSceneActivation = true;
            }

            float resA = Mathf.Clamp01(Time.time / time);
            float resB = Mathf.Clamp01(asyncLoad.progress / 0.9f);
            float progress = resA * resB;
            image.fillAmount = progress;
            yield return null;
        }
    }
}
