﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinCosTest : MonoBehaviour
{
    public float x, y, z;
    public float deltaTest;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        deltaTest += Time.deltaTime;
       x = Mathf.Sin(deltaTest);
        y += Mathf.Cos(deltaTest);
      

        transform.position = new Vector3(x,y,z);
    }
}
