﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Nauka : MonoBehaviour
{
    public enum DiscountType
    {
        Indefinite,
        Temporary
    }

    public bool routine = false;
    public DiscountType discountType = DiscountType.Temporary;
    public float discount = 20.0f;
    public bool enableDiscount;
    public bool clearanceSale = false;
    private DateTime startDiscountTime;
    private DateTime endDiscountTime;
    public float price = 1000.0f;
  

    public float GetPrice()
    {
        if(clearanceSale)
        {
            return price - (price * discount * 0.01f);
        }

        return price;
    }

    public float GetPriceDiscount()
    {
        if(clearanceSale)
        {
            return price * discount * 0.01f;
        }

        return 0;
    }

    private void Update()
    {
        discount = Mathf.Clamp(discount, 0.0f, 100.0f);
    }

    public bool test;

    private void OnGUI()
    {
        if (enableDiscount)
        {
            if (DateTime.Now > startDiscountTime && endDiscountTime < DateTime.Now)
            {
                if (!clearanceSale)
                {
                    endDiscountTime = DateTime.Now.AddSeconds(25);
                    clearanceSale = true;
                }
                else
                {
                    if (endDiscountTime < DateTime.Now)
                    {
                        if (routine)
                        {
                            startDiscountTime = DateTime.Now.AddSeconds(10);
                            enableDiscount = true;
                        }
                        else
                        {
                            enableDiscount = false;
                        }                 

                        clearanceSale = false;
                    }
                }
            }
            else
            {
                Debug.Log("yolo");
            }
        }
        else
        {
            clearanceSale = false;
        }

        GUILayout.Space(50);

        if (GUILayout.Button("Prev sale"))
        {
            enableDiscount = true;
            startDiscountTime = DateTime.Now.AddSeconds(10);
            endDiscountTime = new DateTime();
           
        }

        if(GUILayout.Button("change time start"))
        {
            startDiscountTime = startDiscountTime.AddSeconds(10);
        }

        GUILayout.Box("Start time: " + ((startDiscountTime - DateTime.Now < TimeSpan.Zero) ? "-" : "" + (startDiscountTime - DateTime.Now)));
        GUILayout.Box("End time: " + ((endDiscountTime - DateTime.Now < TimeSpan.Zero) ? "-" : "" + (endDiscountTime - DateTime.Now)));
        GUILayout.Box(string.Format("Price: {0:C}", GetPrice()));
        GUILayout.Box(string.Format("Save: {0:C}", GetPriceDiscount()));
    }
}
