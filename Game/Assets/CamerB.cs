﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamerB : MonoBehaviour
{
    public ManeuverController target;
    public ParticleSystem stardust;

    public float distance = 3.0f;
    public float height = 3.0f;
    public float damping = 5.0f;
    public bool smoothRotation = true;
    public bool followBehind = true;
    public float rotationDamping = 10.0f;
    public Vector3 off;
    public Vector3 oldPos;
   
    void Update()
    {

        if(target.speed > 0.1)
        {
            stardust.Play();
        }
        else
        {
            stardust.Stop();
        }

        oldPos = target.currentPosition;

        Vector3 wantedPosition;
        if (followBehind)
            wantedPosition = target.transform.TransformPoint(0, height, -distance);
        else
            wantedPosition = target.transform.TransformPoint(0, height, distance);

        transform.position = wantedPosition + off;

        transform.rotation = Quaternion.LookRotation(target.oldVelocity);

        stardust.startLifetime = 10f * (target.speed / target.maxSpeed) / 5f;
        stardust.startSpeed = (10f * target.speed) / 2F;

        stardust.emissionRate = 50f * target.speed / target.maxSpeed;
    }
}
