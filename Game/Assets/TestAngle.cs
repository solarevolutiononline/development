﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestAngle : MonoBehaviour
{

 
    public Text speedText;
    public float shipMaxSpeed;
    public float shipSpeedAcceleration;
    public float shipSpeedDeceleration;
    public float inertiaCompensation;
    private float shipCurrentSpeed;
    public float velocityMagnitude;
    private float x, y, z;

    public Quaternion euler;

    public Vector3 currentLinearSpeed;
    public Vector3 prevLinearSpeed;
    public Vector3 prevRotation;
    private Vector3 prevPosition;
    private Vector3 currentPosition;
    private Vector3 velocity;
    public float speedX, speedY, speedZ;
    public float maxLinearSpeed;

    private void Start()
    {
        Application.targetFrameRate = 60;
    }

    public float driftValue;
    public float driftAngle;

    public float ex, ey, ez;
    public void Update()
    {

        x = Input.GetAxis("Vertical") * speedX  * Time.deltaTime;
        y = Input.GetAxis("Horizontal") * speedY * Time.deltaTime;
        z = -Input.GetAxis("Roll") * speedZ * Time.deltaTime;

        if(Input.GetKey(KeyCode.Alpha1))
        {
            maxLinearSpeed += 10f;
        }

        if(Input.GetKey(KeyCode.Alpha2))
        {
            maxLinearSpeed -= 25f;

            maxLinearSpeed = Mathf.Min(maxLinearSpeed, 0f);
        }

        if (Input.GetKey(KeyCode.Space))
        {
           shipCurrentSpeed = Mathf.Clamp(shipCurrentSpeed + shipSpeedAcceleration * Time.deltaTime, 0f, shipMaxSpeed);
        }
        else
        {
            shipCurrentSpeed = Mathf.Clamp(shipCurrentSpeed - shipSpeedAcceleration * Time.deltaTime, 0f, shipMaxSpeed);
        }

        Vector3 euler = new Vector3(x, y, z);
        transform.rotation *= Quaternion.Euler(euler);
     //   currentLinearSpeed = Vector3.ClampMagnitude(currentLinearSpeed, shipMaxSpeed + maxLinearSpeed);
        transform.position = transform.position + (currentLinearSpeed) * Time.deltaTime;


        currentPosition = transform.position;
        velocity = (currentPosition - prevPosition) / Time.deltaTime;
        velocityMagnitude = velocity.magnitude;



        currentLinearSpeed = AdvanceLinearSpeed();

        prevLinearSpeed = currentLinearSpeed;

        delta = transform.forward - prevRotation;
        prevRotation = transform.rotation * Vector3.forward;

        prevPosition = currentPosition;


        speedText.text = Mathf.Round(velocity.magnitude) + " m/s^2";
    }

    public Vector3 delta;

    public bool te;
    public Vector3 AdvanceLinearSpeed()
    {
        Vector3 linearSpeed = prevLinearSpeed;
        Vector3 result = linearSpeed;

        Vector3 forward = prevRotation;
        float num = Vector3.Dot(velocity, forward);

        Vector3 b = forward * num;
        Vector3 vector = linearSpeed - b;
        float magnitude = vector.magnitude;
        Vector3 a = vector.normalized * GetNewSpeed2(magnitude, 0f, inertiaCompensation);

        Vector3 b2 = forward * GetNewSpeed(num, shipCurrentSpeed, shipSpeedAcceleration);
        result = a + b2;

        return result;
    }

    public  float GetNewSpeed(float current, float target, float acceleration)
    {
       
        float num = acceleration * 0.1f;
        float num2 = current - target;

       // Debug.Log("curr-t: " + Mathf.Abs(num2) + " acc: " + num);

        if (Mathf.Abs(num2) < num)
        {
            return target;

        }


        if (num2 > 0f)
        {
            return current - num;
        }

        return current + num;
    }

    public float GetNewSpeed2(float current, float target, float acceleration)
    {
        float num = acceleration * 0.1f;
        float num2 = current - target;

        Debug.Log("Current speed: " + Mathf.Abs(current) + "acceleration: " + num);

        if (Mathf.Abs(num2) < num)
        {
            return target;

        }

        if (num2 > 0f)
        {
            return current - num;
        }

        return current + num;
    }

}
