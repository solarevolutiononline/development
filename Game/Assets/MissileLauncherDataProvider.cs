﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileLauncherDataProvider : MonoBehaviour
{
    public static MissileLauncherDataProvider instance;
    public GameObject miniGunPrefab;
    public float startDistance;
    public float startSpeed;
    public float maxSpeed;
    public float acceleration;
    public float trunSpeed;

    void Awake()
    {
        instance = this;
    }
}
