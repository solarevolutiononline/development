﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckAngle : MonoBehaviour
{
    public Transform target;


    public List<GameObject> slots;

    public float gunAngle;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public float GetAngle(float angle)
    {
        if (angle < 10f)
        {
            return 20f;
        }
        if (angle < 15f)
        {
            return 30f;
        }
        if (angle < 20f)
        {
            return 40f;
        }
        if (angle < 25f)
        {
            return 50f;
        }
        if (angle < 30f)
        {
          return 60f;
        }
        if (angle < 38f)
        {
           return 75f;
        }
        if (angle < 46f)
        {
            return 90f;
        }
        if (angle < 61f)
        {
            return 120f;
        }
        if (angle < 91f)
        {
          return 180f;
        }

        return 360f;
    }

    private float CalcAngle(Vector3 vector1, Vector3 vector2)
    {
        float num = Vector3.Angle(vector1, vector2);
        if (Vector3.Cross(vector1, vector2).y > 0f)
        {
            return num;
        }
        return 360f - num;
    }


    private void OnGUI()
    {
        foreach(var o in slots)
        {
            float angle = Vector3.Angle(o.transform.forward, (target.position - o.transform.position).normalized);

            GUILayout.Box(o.name + " " + angle + " mozna strzelac: " + (angle <= GetAngle(gunAngle)));
        }
        //GUILayout.Box("Can shot: "+ (angle <= gunAngle));
        //GUILayout.Box("Angle: " + angle);

        //GUILayout.Box("gun angle: " + GetAngle(gunAngle));
    }
}
