﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionTest : MonoBehaviour
{
    public TestSpaceObject playerA;
    public TestSpaceObject playerB;

    public float a, b;
    private void Update()
    {
        // CheckCollision(playerA, playerB);
        playerA.Update();
        playerB.Update();

       CheckCollision(playerA, playerB);
        CheckCollision(playerB, playerA);
        //    CheckCollision(playerB, playerA);
    }

    public void CheckCollision(TestSpaceObject a, TestSpaceObject b)
    {


        bool collisionX = a.Position.x + a.Size.x / 2f + b.Size.x / 2f >= b.Position.x && b.Position.x + b.Size.x / 2f + a.Size.x / 2f >= a.Position.x;
        bool collisionY = a.Position.y + a.Size.y / 2f + b.Size.y / 2f >= b.Position.y && b.Position.y + b.Size.y / 2f + a.Size.y / 2f >= a.Position.y;
        bool collisionZ = a.Position.z + a.Size.z / 2f + b.Size.z / 2f >= b.Position.z && b.Position.z + b.Size.z / 2f + a.Size.z / 2f >= a.Position.z;


        if(collisionX && collisionY && collisionZ)
        {
            Debug.Log("Collision");
        }

    }

    public void Test(TestSpaceObject a, TestSpaceObject b)
    {
        if (a.Position.x < b.Position.x + b.Size.x && a.Position.x + a.Size.x > b.Position.x && a.Position.y < b.Position.y + b.Size.y && a.Position.y + a.Size.y > b.Position.y &&
             a.Position.z < b.Position.z + b.Size.z && a.Position.z + a.Size.z > b.Position.z)
        {
            Debug.Log("collision detected!");
        }
    }

    public bool CheckC(TestSpaceObject a, TestSpaceObject b)
    {
    

        //check the X axis
        if (Mathf.Abs(a.Position.x - b.Position.x) < a.Size.x + b.Size.x)
        {
            //check the Y axis
            if (Mathf.Abs(a.Position.y - b.Position.y) < a.Size.y + b.Size.y)
            {
                //check the Z axis
                if (Mathf.Abs(a.Position.z - b.Position.z) < a.Size.z + b.Size.z)
                {
                    
                    return true;
                }
            }
        }

        return false;
    }
}


//Vector3 relativeVelocity = a.velocity - b.velocity;

//float magnitude2 = Vector3.Cross(relativeVelocity.normalized, a.velocity.normalized).magnitude;

//bool collisionY = Mathf.Abs(a.Position.y) + a.Size.y / 2f + b.Size.z / 2f >= Mathf.Abs(b.Position.y) && Mathf.Abs(b.Position.y) + b.Size.y / 2f + a.Size.y / 2f >= Mathf.Abs(a.Position.y);
//bool collisionZ = Mathf.Abs(a.Position.z) + a.Size.z / 2f + b.Size.y / 2f >= Mathf.Abs(b.Position.z) && Mathf.Abs(b.Position.z) + b.Size.z / 2f + a.Size.z / 2f >= Mathf.Abs(a.Position.z);

[System.Serializable]
public class TestSpaceObject
{
    public Vector3 Size = new Vector3(1f, 1f, 1f);
    public GameObject root;
    public bool move;
    public float speed = 10f;
    public Vector3 Position
    {
        get
        {
            return root.transform.position;
        }
    }

    public Vector3 oldPosition { get; set; }
    public Vector3 currentPosition { get; set; }
    public Vector3 velocity { get; set; }

    public Vector3 forceSpeed { get; set; }

    public void Update()
    {
        if(move)
        {
          //  speed = Mathf.Clamp(speed + 5f * Time.deltaTime, speed, 10f);
            
            root.transform.position += (-root.transform.right + forceSpeed) * speed * Time.deltaTime;
        }

        forceSpeed = Vector3.Lerp(forceSpeed, Vector3.zero, 0.5f * Time.deltaTime);
        currentPosition = root.transform.position;
        velocity = (currentPosition - oldPosition) / Time.deltaTime;
        oldPosition = currentPosition;
        root.transform.localScale = Size;
    }
   
}
