﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugCamera : MonoBehaviour
{
    public Movement playerScript;

    public Transform player;
    private Quaternion wantedRotation;
    public float z = -1;
    public float rotationTime = 3.5f;
    private void Start()
    {
        
    }

    private void Update()
    {
        Vector3 forward = player.forward;
        wantedRotation = Quaternion.LookRotation(forward, player.up);

    }

    private void LateUpdate()
    {

        Quaternion toRot = Quaternion.LookRotation(player.position - transform.position, player.up);
        transform.rotation = Quaternion.Slerp(transform.rotation, toRot, playerScript.movementFrame.euler3.yaw * Time.deltaTime);


        Vector3 toPos = player.position + (player.rotation * new Vector3(0f, 0f, z));

        Vector3 curPos = Vector3.Lerp(transform.position, toPos, playerScript.speed * Time.deltaTime);
        transform.position = curPos;

     

    }
}
