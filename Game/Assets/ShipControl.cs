using System;
using UnityEngine;

[System.Serializable]
public class PlayerStats
{
    public static PlayerStats instance;

    [Header("Stats")]
    public new string name = "Mingebag";

    public int money = 150;

    public int health = 1500;

    public int maxHealth = 1500;

    public int maxcargo = 2500;

    public int energy = 2000;

    public int maxEnergy = 2000;

  //  public List<Item> inventory = new List<Item>();

    [Header("Controls")]
    public float maxSpeed = 30f;

    public float acceleration = 3f;

    public float yawPitchFactor = 1.2f;

    public float rollFactor = 1.2f;

    [Header("Weapons")]
    public GameObject weaponSlot1;

    public string wS1Name = string.Empty;

    public int wS1Num;

    public GameObject weaponSlot2;

    public string wS2Name = string.Empty;

    public int wS2Num;

    public GameObject[] auxSlot = new GameObject[4];

    public string[] auxName = new string[4];

    public int[] auxNum = new int[4];

    [Header("Other")]
    public bool spawnInNextScene;

    public int spawnPositionIndex = 1;

    public GameObject playerPrefab;

    public GameObject player;

    public bool spawned;

    public string currentLevel = string.Empty;

    public bool[] openedLocations = new bool[4];

    public bool populateLocations;

    [Header("Quests")]
    public bool hasActiveQuest;

    public string[] questsBase1;

    public string[] questsBase2;

    public int base1QuestNum;

    public int base2QuestNum;

}
public class ShipControl : MonoBehaviour
{
	public bool enable = true;

	private Rigidbody body;

	private Quaternion desRotation;

	public Transform checkStartPos;

	public LayerMask mask;

	private GameObject selectedTarget;

	public GameObject selectionMarkerFab;

	//private SelectionMarker selMarker;

	public AudioClip selectTg;

	public AudioClip deselectTg;

	public AudioClip rockCollision;

	public AudioClip otherCollision;

	private AudioSource audiosource;

	public float MaxFwdSpd = 100f;

	public float MaxBwdSpd = -20f;

	public float Acceleration = 3f;

	public float Decceleration = 1f;

	private float yawPitchFactor = 1f;

	private float rollFactor = 1.5f;

	private float curSpd;

	public float trgSpd;

	public PlayerStats stats;

	private int activeAuxSlot;

	private Vector3 controlVector = Vector3.zero;

	public float mouseSensitivity = 0.1f;

	private bool first = true;

	private float horAxis;

	private float vertAxis;

	private float rollAxis;

	private void Init()
	{
		this.audiosource = base.GetComponent<AudioSource>();
		this.body = base.GetComponent<Rigidbody>();
		this.desRotation = base.transform.rotation;
	//	this.SetWeaponGUIInfo();
		//this.stats = PlayerStats.instance;
		this.MaxFwdSpd = 10;
		this.Acceleration = this.stats.acceleration;
		this.yawPitchFactor = this.stats.yawPitchFactor;
		this.rollFactor = this.stats.rollFactor;
	}

	//private void Update()
	//{
	//	if (this.first)
	//	{
	//		this.Init();
	//		this.first = false;
	//	}
	//	if (this.selectedTarget != null)
	//	{
	//		this.UpdateTargetInfo();
	//	}
	//	else
	//	{
	//		this.ClearTargetUIInfo();
	//	}
	//	Debug.DrawRay(this.checkStartPos.position, base.transform.TransformDirection(new Vector3(1f, 0f, 0f)), Color.red);
	//	if (this.enable)
	//	{
	//		if (Input.GetKeyDown("1"))
	//		{
	//			this.activeAuxSlot = 0;
	//			this.SetWeaponGUIInfo();
	//		}
	//		if (Input.GetKeyDown("2"))
	//		{
	//			this.activeAuxSlot = 1;
	//			this.SetWeaponGUIInfo();
	//		}
	//		if (Input.GetKeyDown("3"))
	//		{
	//			this.activeAuxSlot = 2;
	//			this.SetWeaponGUIInfo();
	//		}
	//		if (Input.GetKeyDown("4"))
	//		{
	//			this.activeAuxSlot = 3;
	//			this.SetWeaponGUIInfo();
	//		}
	//		if (Input.GetMouseButton(0))
	//		{
	//			if (this.selectedTarget != null)
	//			{
	//				IWeapon component = this.stats.weaponSlot1.GetComponent<IWeapon>();
	//				if (component != null)
	//				{
	//					component.Shoot(this.selectedTarget.transform);
	//				}
	//				IWeapon component2 = this.stats.weaponSlot2.GetComponent<IWeapon>();
	//				if (component2 != null)
	//				{
	//					component2.Shoot(this.selectedTarget.transform);
	//				}
	//			}
	//			else
	//			{
	//				IWeapon component3 = this.stats.weaponSlot1.GetComponent<IWeapon>();
	//				if (component3 != null)
	//				{
	//					component3.Shoot(null);
	//				}
	//				IWeapon component4 = this.stats.weaponSlot2.GetComponent<IWeapon>();
	//				if (component4 != null)
	//				{
	//					component4.Shoot(null);
	//				}
	//			}
	//		}
	//		if (Input.GetMouseButtonDown(1))
	//		{
	//			this.LockOn();
	//		}
	//		if (Input.GetKey("space"))
	//		{
	//		///	this.SetWeaponGUIInfo();
	//			if (this.stats.auxSlot[this.activeAuxSlot] != null)
	//			{
	//				if (this.selectedTarget != null)
	//				{
	//					this.stats.auxSlot[this.activeAuxSlot].GetComponent<IWeapon>().Shoot(this.selectedTarget.transform);
	//				}
	//				else
	//				{
	//				//	this.stats.auxSlot[this.activeAuxSlot].GetComponent<IWeapon>().Shoot(null);
	//				}
	//			}
	//		}
	//		if (Input.GetKeyDown("r"))
	//		{
	//		}
	//	}
	//}




	private void LockOn()
	{
		Vector3 direction = Camera.main.transform.TransformDirection(Vector3.forward);
		Vector3 origin = Camera.main.ScreenToWorldPoint(new Vector3((float)(Screen.height / 2), (float)(Screen.width / 2), 0f));
		origin.z = Camera.main.transform.position.z;
		RaycastHit h;
		if (Physics.SphereCast(origin, 0.1f, direction, out h, 500f, this.mask))
		{
		//	this.SelectTarget(h);
		}
		else if (Physics.SphereCast(Camera.main.transform.position, 1f, direction, out h, 700f, this.mask))
		{
			//this.SelectTarget(h);
		}
		else
		{
		//	this.DeselectTarget();
		}
	}

    public float x, y, z;

	private void FixedUpdate()
	{
		if (this.first)
		{
			this.Init();
			this.first = false;
		}
		if (this.enable)
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
			this.horAxis = Input.GetAxis("Roll");
			this.vertAxis = Input.GetAxis("Horizontal");
			this.rollAxis = Input.GetAxis("Vertical");
			this.horAxis = Mathf.Clamp(this.horAxis, -this.yawPitchFactor, this.yawPitchFactor);
			this.vertAxis = Mathf.Clamp(this.vertAxis, -this.yawPitchFactor, this.yawPitchFactor);
			this.controlVector.z = -controlVector.z + this.horAxis * this.mouseSensitivity;
			this.controlVector.y = -this.controlVector.y - this.vertAxis * this.mouseSensitivity;
			this.controlVector.x = this.rollAxis * this.rollFactor;
			this.desRotation *= Quaternion.Euler(new Vector3(-this.rollAxis * this.rollFactor, -this.vertAxis, this.horAxis));
			this.body.angularVelocity = base.transform.TransformDirection(this.controlVector);


   //         if (Input.GetKey("w"))
			//{
			//	this.trgSpd += this.Acceleration * Time.deltaTime;
			//	this.trgSpd = Mathf.Clamp(this.trgSpd, this.MaxBwdSpd, this.MaxFwdSpd);
			//}
			//else if (Input.GetKey("s"))
			//{
			//	if (this.trgSpd > 0f)
			//	{
			//		if (this.trgSpd > 0.3f)
			//		{
			//			if ((double)this.trgSpd < 0.1 * (double)this.MaxFwdSpd)
			//			{
			//				this.trgSpd -= this.Acceleration * Time.deltaTime;
			//			}
			//			else
			//			{
			//				this.trgSpd *= 0.98f;
			//			}
			//		}
			//		else
			//		{
			//			this.trgSpd = 0f;
			//		}
			//	}
			//	else
			//	{
			//		this.trgSpd -= this.Acceleration * Time.deltaTime;
			//	}
			//	this.trgSpd = Mathf.Clamp(this.trgSpd, this.MaxBwdSpd, this.MaxFwdSpd);
			//}
			//else if (Input.GetKey("x"))
			//{
			//	if (Mathf.Abs(this.trgSpd) > 0.3f)
			//	{
			//		if ((double)this.trgSpd < 0.1 * (double)this.MaxFwdSpd && this.trgSpd > 0f)
			//		{
			//			this.trgSpd -= this.Acceleration * Time.deltaTime;
			//		}
			//		else
			//		{
			//			this.trgSpd *= 0.98f;
			//		}
			//	}
			//	else
			//	{
			//		this.trgSpd = 0f;
			//	}
			//}
		}
		this.controlVector *= 0.96f;
		this.body.velocity = base.transform.TransformDirection(new Vector3(this.trgSpd, 0f, 0f));
	//	GUIManager.instance.SetSpeed(Mathf.Abs(this.trgSpd) / this.MaxFwdSpd);
	}

	private void OnCollisionEnter(Collision col)
	{
		Vector3 normal = col.contacts[0].normal;
		Vector3 normalized = this.body.velocity.normalized;
		float magnitude = Vector3.Cross(normalized, normal).magnitude;
		float num = col.relativeVelocity.magnitude / this.MaxFwdSpd + 0.25f;
		num = Mathf.Clamp(num, 0f, 1f);
		if (col.collider.attachedRigidbody != null)
		{
			this.audiosource.PlayOneShot(this.otherCollision, num);
			Vector3 vector = col.collider.attachedRigidbody.mass * col.relativeVelocity;
			Vector3 vector2 = this.body.mass * this.body.velocity;
			if (vector2.magnitude > 1f)
			{
				float num2 = vector.magnitude / vector2.magnitude;
				num2 = Mathf.Clamp(num2, 0f, 1f);
				this.trgSpd -= this.trgSpd * num2;
			}
			else
			{
				this.trgSpd = 0f;
			}
		}
		else
		{
			this.audiosource.PlayOneShot(this.rockCollision, num);
			int amount = 0;
			int num3 = 35;
			float magnitude2 = Vector3.Cross(col.relativeVelocity.normalized, this.body.velocity.normalized).magnitude;
			if (col.relativeVelocity.magnitude > 3f)
			{
				amount = Mathf.CeilToInt((float)num3 * magnitude2 * Mathf.Abs(this.trgSpd));
			}
			if (magnitude2 < 0.5f)
			{
				this.trgSpd -= Mathf.Clamp(0.1f + magnitude2, 0f, 1f) * this.trgSpd;
			}
			else
			{
				this.trgSpd = 0f;
			}
			//base.GetComponent<IDamageReciever>().DoDamage(amount);
		}
	}
}
