﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Root : MonoBehaviour
{
    public SpaceObject spaceObject;

    void Update()
    {
        spaceObject.Update();        
    }
}
