﻿using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System;
using System.Collections.Generic;

public class Connection
{
    public enum State
    {
        Connecting,
        Connected,
        Disconnected
    }

    public delegate void ConnectionCallback(string message);
    public event ConnectionCallback OnConnectionDrooped = delegate{ };
    private Socket handler;
    private DateTime lastActivity;
    private State state = State.Disconnected;
    private bool canSend;
    private bool canReceive;
    private Ping ping;
    private float nextPingTime;
    public bool isLive;

    public uint totalReceivedData;
    public uint totalSentData;
    public int needToReceive;

    public bool IsConnected
    {
        get
        {
            return handler != null && handler.Connected;
        }
    }

    public int Ping
    {
        get
        {
            if (ping != null)
            {
                return ping.time;
            }

            return -1;
        }
    }

    public void Connect(string ip, int port)
    {
        try
        {
            state = State.Connecting;
            handler = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            handler.ReceiveBufferSize = 65536;
            handler.SendBufferSize = 65536;
            handler.NoDelay = true;
            handler.Blocking = true;

            IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse((isLive) ? "80.211.204.157" : ip), port);
            handler.Connect(remoteEP);
            state = State.Connected;
            canSend = true;
            canReceive = true;
        }
        catch (Exception ex)
        {
           this.OnConnectionDrooped(ex.Message);
        }
    }

    public ProtocolReader Receive()
    {
        try
        {
            if (handler != null && handler.Connected)
            {
                if (Time.realtimeSinceStartup > nextPingTime)
                {
                    nextPingTime = Time.realtimeSinceStartup + 5;
                    ping = new Ping((isLive) ? "80.211.204.157" : "127.0.0.1");
                }
                if (handler.Poll(500, SelectMode.SelectRead))
                {
                    byte[] buffer = new byte[1];

                    if (handler.Receive(buffer, SocketFlags.Peek) == 0)
                    {
                        throw new Exception("The socket has been closed");
                    }
                    else
                    {
                        lastActivity = DateTime.Now;

                        byte[] buffer2 = new byte[4];

                        if (handler.Receive(buffer2, 0, buffer2.Length, SocketFlags.None) != 4)
                        {
                            throw new Exception("Error receiving: packetLenght != 4");
                        }

                        int packetLenght = BitConverter.ToInt32(buffer2, 0);
                        totalReceivedData += (uint)packetLenght;

                        byte[] buffer3 = new byte[packetLenght];

                        if (handler.Receive(buffer3, 0, buffer3.Length, SocketFlags.None) != packetLenght)
                        {
                            throw new Exception("Error receiving: receive length != packet lenght");
                        }

                        return new ProtocolReader(buffer3);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Disconnect(ex.Message);
        }

        return null;
    }


    public List<ProtocolReader> ReceiveMessages()
    {
        try
        {
            if (handler != null && handler.Connected)
            {
                if (Time.realtimeSinceStartup > nextPingTime)
                {
                    nextPingTime = Time.realtimeSinceStartup + 5;
                    ping = new Ping((isLive) ? "80.211.204.157" : "127.0.0.1");
                }
                if (handler.Poll(0, SelectMode.SelectRead))
                {
                    byte[] buffer = new byte[1];

                    if (handler.Receive(buffer, SocketFlags.Peek) == 0)
                    {
                        throw new Exception("The socket has been closed");
                    }
                    else
                    {
                        List<ProtocolReader> pr = new List<ProtocolReader>();

                        while(handler.Available != 0)
                        {

                            lastActivity = DateTime.Now;

                            byte[] buffer2 = new byte[4];

                            if (handler.Receive(buffer2, 0, buffer2.Length, SocketFlags.None) != 4)
                            {
                                throw new Exception("Error receiving: packetLenght != 4");
                            }

                            int packetLenght = BitConverter.ToInt32(buffer2, 0);
                            totalReceivedData += (uint)packetLenght;

                            byte[] buffer3 = new byte[packetLenght];

                            if (handler.Receive(buffer3, 0, buffer3.Length, SocketFlags.None) != packetLenght)
                            {
                                throw new Exception("Error receiving: receive length != packet lenght");
                            }

                            pr.Add(new ProtocolReader(buffer3));
                        }

                        return pr;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Disconnect(ex.Message);
        }

        return null;
    }

    public void SendMessage(ProtocolWriter protocolWriter)
    {
        try
        {
            if (!canSend)
            {
                throw new SocketException(10057);
            }

            int lenght = protocolWriter.GetLenght();
            totalSentData += (uint)lenght;
        
            byte[] array = new byte[4 + lenght];
            byte[] bytes = BitConverter.GetBytes(lenght);
            Array.Copy(bytes, 0, array, 0, bytes.Length);
            Array.Copy(protocolWriter.GetBuffer(), 0, array, 4, protocolWriter.GetLenght());
            handler.Send(array, 0, array.Length, SocketFlags.None);
        }
        catch (Exception ex)
        {
            Disconnect(ex.Message);
        }
    }

    public void Disconnect(string text)
    {
        state = State.Disconnected;
        canSend = false;
        canReceive = false;

        if (handler != null)
        {
            handler.Close();
            handler = null;
        }

        this.OnConnectionDrooped(text);
    }
}
