﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngleTest : MonoBehaviour
{

    public Transform playerShip;
    // Start is called before the first frame update
    void Start()
    {

    }

    public float arc;
  

    void OnGUI()
    {

        Vector3 targetDir = playerShip.transform.position - transform.position;
        targetDir = targetDir.normalized;

        float angle = CalcAngle0to360CW2(transform.forward, targetDir);

        GUILayout.Box("Angle: " +  angle + " flag: "+ ((angle <= arc) || arc == 360f));
     //   GUILayout.Box("Angle: " + CalcAngle0to360CW2(transform.forward, targetDir));
        GUILayout.Box("dot: " + Vector3.Dot(transform.right, targetDir));
    
    }



    private float CalcAngle0to360CW(Vector3 vector1, Vector3 vector2)
    {
        float num = Vector3.Angle(vector1, vector2);

        if (Vector3.Cross(vector1, vector2).y > 0f)
        {
            return 180;
        }
        return num;
    }


    private float CalcAngle0to360CW2(Vector3 vector1, Vector3 vector2)
    {
        float num = Vector3.Angle(vector1, vector2);

        if (arc < 180)
        {
            return num;
        }
        else
        {
            if (Vector3.Cross(vector1, vector2).y > 0f)
            {
                return num;
            }

            return 360;
        }
    }
}
