﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSTest2 : MonoBehaviour
{
    public Text fpsText;
   
    private void Awake()
    {
     
    }

    public void Start()
    {
        Debug.Log("Awake");
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 2000;
        Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, true);

        startTime = Time.realtimeSinceStartup;
    }

    public float time, nextUpdate, frameCount;
    int updateCount;
    private float fps;

    public float startTime;

    void Update()
    {

       // ActionFPS();

        if (Time.realtimeSinceStartup - this.time >= 1)
        {

            this.frameCount++;
            this.time += Time.deltaTime;
            if (Time.time <= this.nextUpdate)
            {
                return;
            }
            this.nextUpdate = Time.time + 1f;
            this.fps = (float)this.frameCount / this.time;
            int num = this.updateCount + 1;
            this.updateCount = num;
            if (num > 4)
            {
                this.updateCount = 0;
                this.frameCount = 0;
                this.time = 0f;
            }
            this.time = Time.realtimeSinceStartup;

            fpsText.color = (fps > 60f) ? Color.green : (fps > 30f) ? Color.yellow : Color.red;
            fpsText.text = "FPS: " + Mathf.Ceil(fps);
        }

        
    }


    void ActionFPS()
    {

        this.frameCount++;
        this.time += Time.deltaTime;
        if (Time.time <= this.nextUpdate)
        {
            return;
        }
        this.nextUpdate = Time.time + 1f;
        this.fps = (float)this.frameCount / this.time;
        int num = this.updateCount + 1;
        this.updateCount = num;
        if (num > 4)
        {
            this.updateCount = 0;
            this.frameCount = 0;
            this.time = 0f;
        }
    }

    private void OnGUI()
    {
      
    }
}