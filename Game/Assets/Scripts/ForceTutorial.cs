﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceTutorial : MonoBehaviour
{
   
    public float x, y, z;
    public Transform target;
    public ForceTutorial tForceTutorial;
    public float currentSpeed = 0f;
    public float maxSpeed;
    public float speedAcceleration;
    [Header("----------")]
    public Vector3 currentPos;
    public Vector3 prevPos;
    public Vector3 velocity;
    public float veloMagnitude;
    [Header("----------")]
    public Vector3 dir;
    public Vector3 force;
    public float time;
    [Header("----------")]
    public float mass = 1000;
    public bool movement;
    public float test;
    public float fForwardSpeed;
    public float fTargetSpeed;
    
    private void Start()
    {
        prevPos = transform.position;
    }


    public Vector3 GetRelativeVelocity(Vector3 a, Vector3 b)
    {
        return a - b;
    }
    void Update()
    {
        if (movement)
        {
            x = Input.GetAxis("Vertical");
            y = Input.GetAxis("Horizontal");
            z = -Input.GetAxis("Roll");
        }

        currentSpeed = Mathf.Clamp(currentSpeed + speedAcceleration * Time.deltaTime, 0f, maxSpeed);

      //fForwardSpeed = Vector3.Dot(transform.forward, velocity);


      //  if (fForwardSpeed > fTargetSpeed)
      //      force = new Vector3(0f, 0f, -5f);
      //  else
      //      force = new Vector3(0f, 0f, 5f);

        transform.position += transform.forward  * (currentSpeed * 0.1f - (1f * force.magnitude)) * Time.deltaTime;
        transform.rotation *= Quaternion.Euler(x, y, z);



        //currentPos = transform.position;
        //velocity = (currentPos - prevPos) / Time.deltaTime;

        //prevPos = currentPos;
        //veloMagnitude = velocity.magnitude;
        //time = currentPos.magnitude / velocity.magnitude;



     
    }

  

    //private void OnGUI()
    //{

    //    GUILayout.Box("Velocity: " + velocity.magnitude);
    //    if(GUILayout.Button("Add force"))
    //    {
    //        force = new Vector3(0f, 0f, -10f);
   
    //    }
    //}
}
