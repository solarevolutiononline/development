﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;
using System.Net;

public class Launcher : MonoBehaviour
{
    private static Launcher instance;
    private Connection connection;
 //   private LoginProtocol loginProtocol;
    public string ip = "127.0.0.1";
    public int port = 27050;
    public bool isLive;
    public bool maintenance;
    public bool autoReconnect;
    public DateTime nextReconnect;
    public bool isLogged;
    private bool isUpdating;
    public Text statusText;
    public Text pingText;
    public Image patchHandleImage;
    public Text patchText;
    public Text idText;
    public Text usernameText;
    public Text messageText;
    public InputField usernameField;
    public InputField passwordField;
    public Button forgotPasswordButton;
    public Button logoutButton;
    public Button loginButton;
    public Button registerButton;
    public Button playButton;
    public GameObject characterWindow;
    public Launcher.ErrorWindow errorWindow;

    public static Launcher GetInstance()
    {
        return instance;
    }

    private void Start()
    {

        Application.targetFrameRate = -1;
        QualitySettings.vSyncCount = 0;
 


        instance = this;

        connection = new Connection();
        connection.isLive = isLive;
        connection.Connect(ip, port);
        connection.OnConnectionDrooped += Connection_OnConnectionDrooped;
        nextReconnect = DateTime.Now.AddSeconds(5);

     //   loginProtocol = new LoginProtocol();
    //    loginProtocol.SetConnection(connection);

//        Patcher patcher = Patcher.CreateNew();
      }

    public float time;
    public float patchTime = 100;
    public Image img;
    public Text tes;

    IEnumerator Patch()
    {
      //  yield return new WaitForSeconds(5);

        patchText.text = "Checking files...";

        yield return new WaitForSeconds(1);

        isUpdating = true;

        while (img.fillAmount != 1)
        {
            img.fillAmount = time / patchTime;
            time += UnityEngine.Random.Range(1f, 10f) ;

            tes.text = (int)(img.fillAmount  * 100f) + "%";

            yield return new WaitForSeconds(0.01f);
        }

        yield return new WaitForSeconds(1);
        patchText.text = "Game is ready! ";
        isUpdating = false;
    }

    private void Update()
    {
      //  Debug.Log(Time.deltaTime);
        pingText.gameObject.SetActive(connection.IsConnected);
        pingText.text = "PING: " +  connection.Ping.ToString();

        statusText.text = "SERVER STATUS: " + ((maintenance) ? "<color=red>MAINTENANCE</color>" : ((connection.IsConnected) ? "<color=green>ONLINE</color>" : "<color=red>OFFLINE</color>"));
      
        playButton.interactable = !((!connection.IsConnected || maintenance || !isLogged || isUpdating));
      
        if (connection.IsConnected)
        {
            ProtocolReader protocolReader = connection.Receive();

            if(protocolReader != null)
            {
                Protocol.ProtocolType protocolType = (Protocol.ProtocolType)protocolReader.ReadInt32();
                
                //if(loginProtocol.ProtocolType == protocolType)
                //{
                //    loginProtocol.ParseMessage(protocolReader);
                //}
            }
        }
        else if(autoReconnect)
        {
            if (DateTime.Now > nextReconnect)
            {
                nextReconnect = DateTime.Now.AddSeconds(10);
                connection.Connect(ip, port);
            }
        }
    }

    private void Connection_OnConnectionDrooped(string message)
    {
        nextReconnect = DateTime.Now.AddSeconds(5);

        usernameField.gameObject.SetActive(true);
        passwordField.gameObject.SetActive(true);
        forgotPasswordButton.gameObject.SetActive(true);
        registerButton.gameObject.SetActive(true);
        loginButton.gameObject.SetActive(true);
        idText.gameObject.SetActive(false);
        usernameText.gameObject.SetActive(false);
        logoutButton.gameObject.SetActive(false);
        playButton.gameObject.SetActive(false);

        Game.Destroy();
    }

    public void Localhost()
    {
        connection.isLive = false;
        connection.Disconnect("Localhost");
        connection.Connect(ip, port);
    }


    public void Live()
    {
        connection.isLive = true;
        connection.Disconnect("Live");
        connection.Connect(ip, port);
    }


    [Serializable]
    public class ErrorWindow
    {
        public GameObject errorWindow;
        public Text message;

        public void Show(string text)
        {
            message.text = text;
            errorWindow.SetActive(true);  
        }
    }

    public void Login()
    {
      

        if (!connection.IsConnected)
        {
            errorWindow.Show("SERVER STATUS: OFFLINE");
        }

        messageText.text = "";
        // loginProtocol.Login(usernameField.text, passwordField.text);
     //   loginProtocol.Login("Pride_DE", "Test");
    }

    public void Register()
    {
        if (!connection.IsConnected)
        {
            errorWindow.Show("SERVER STATUS: OFFLINE");
        }
    }

    public void Logout()
    {
       // loginProtocol.Logout();
    }

    public void Play()
    {
    //    loginProtocol.Play();
    }

    public void ForgotPassword()
    {
        if (!connection.IsConnected)
        {
            errorWindow.Show("SERVER STATUS: OFFLINE");
        }
    }

    //IEnumerator Wait()
    //{
    //    yield return new WaitForSeconds(1);
    //    Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, true);
    //    Application.LoadLevel("Demo");

    //}

    public void Log(string text)
    {
        Debug.Log(text);
    }

    public void ToggleFullscreen(bool fullscreen)
    {
        Screen.fullScreen = fullscreen;

        if (Screen.fullScreen)
        {
            Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
        }
        else
        {
            Screen.fullScreenMode = FullScreenMode.Windowed;
        }
    }

    public void Show(string text)
    {
        messageText.text = text;
    }

    public void SetSession(int id, string username, string character)
    {
        idText.text = "ID: " + id.ToString();
        usernameText.text = "USERNAME: " + username;
    }

    public void SetLoginState(bool isLogged)
    {
        this.isLogged = isLogged;

        if(isLogged)
        {
            usernameField.gameObject.SetActive(false);
            passwordField.gameObject.SetActive(false);
            forgotPasswordButton.gameObject.SetActive(false);
            registerButton.gameObject.SetActive(false);
            loginButton.gameObject.SetActive(false);
            idText.gameObject.SetActive(true);
            usernameText.gameObject.SetActive(true);
            logoutButton.gameObject.SetActive(true);
            playButton.gameObject.SetActive(true);
            characterWindow.gameObject.SetActive(true);
        }
        else
        {
            usernameField.gameObject.SetActive(true);
            passwordField.gameObject.SetActive(true);
            forgotPasswordButton.gameObject.SetActive(true);
            registerButton.gameObject.SetActive(true);
            loginButton.gameObject.SetActive(true);
            idText.gameObject.SetActive(false);
            usernameText.gameObject.SetActive(false);
            logoutButton.gameObject.SetActive(false);
            playButton.gameObject.SetActive(false);
            characterWindow.gameObject.SetActive(false);
        }
    }

    private void OnApplicationQuit()
    {
        PlayerPrefs.DeleteAll();

        connection.Disconnect("OnApplicationQuit");
    }
}


