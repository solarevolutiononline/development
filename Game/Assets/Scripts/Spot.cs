﻿using UnityEngine;


public class Spot
{
    public int Id { get; set; }
    public Vector3 LocalPosition { get; private set; }
    public Quaternion LocalRotation { get; private set; }

    private Transform transform;
    private GameObject gameObject;
    public float test;
    public Transform Transform
    {
        get
        {
            return transform;
        }
    }

    public Spot(int id, Vector3 localPosition, Quaternion localRotation)
    {
        this.Id = id;
        this.LocalPosition = localPosition;
        this.LocalRotation = localRotation;
    }

    public void Initialize(Root root)
    {
        gameObject = new GameObject("Spot_" + Id);
        transform = gameObject.transform;
        transform.parent = root.transform;
        transform.position = LocalPosition;
        transform.rotation = LocalRotation;
    }
}

[System.Serializable]
public class Weapon
{
    public int SpotId;

    protected Spot spot;

    public float speed = 150f;
    public float reloadTime = 1f;
    public float nextFire;
    public float angle = 45f;

    public float test;
 

    public Weapon ()
    {
       
    }

    public void SetSpot(Spot spot)
    {
        SpotId = spot.Id;
        this.spot = spot;
    }


    public bool CanCast
    {
        get
        {
            return Time.time > nextFire;
        }
    }


    public float TimeLeft
    {
        get
        {
            return Mathf.Clamp(nextFire - Time.time, 0f, nextFire);
        }
    }

    public virtual void Shot(SpaceObject target)
    {

    }
}

[System.Serializable]
public class Gun : Weapon
{

    public Root root;


    public Gun(Root root)
    {
        this.root = root;
    }

    public override void Shot(SpaceObject target)
    {
        nextFire = Time.time + reloadTime;

        if (target.dead)
        {
            return;
        }

        target.health -= Random.Range(34f, 178f);
        if (target.health <= 0)
        {
           // target.health = Random.Range(1000f, 10000f);
            target.Destroy();
            return;
        }
        //root.fxGun
        GameObject bulletObject = GameObject.Instantiate(MissileLauncherDataProvider.instance.miniGunPrefab, spot.Transform.position, spot.Transform.rotation) as GameObject;
        Bullet bullet = bulletObject.AddComponent<Bullet>();
        bullet.target = target.gameObject.transform;
        bullet.startPosition = GameObject.FindObjectOfType<Root>().transform.position + spot.LocalPosition;
        bullet.te = (target.gameObject.transform.position - spot.Transform.position).magnitude / 400f;
        bullet.on = true;
    }
}

public class MissileWeapon : Weapon
{
    public Spot spot;


    public MissileWeapon(Spot spot)
    {
        this.spot = spot;
    }
    public override void Shot(SpaceObject target)
    {
        nextFire = Time.time + reloadTime;

        GameObject missileObject = GameObject.Instantiate(Resources.Load("missile"), spot.Transform.position, spot.Transform.rotation) as GameObject;
        Missile missile = missileObject.AddComponent<Missile>();
        missile.Speed = 75f;
        missile.root = spot.Transform;
        missile.Target = target.gameObject.transform;
  
       missile.tdmg = target;
        missile.In();
        missile.flag = true;
       
    }
}
