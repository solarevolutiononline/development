﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Text;

public class Game : MonoBehaviour
{
    private static Game instance;
    private Connection connection;
    public Dictionary<Protocol.ProtocolType, Protocol> protocols;
    private List<SpaceObject> objects = new List<SpaceObject>();
    public int myId;
   
    public List<SpaceObject> SpaceObjects
    {
        get
        {
            return objects;
        }
    }
    public static Game GetInstance()
    {
        return instance;
    }

    public static Connection Connection
    {
        get
        {
            return instance.connection;
        }
    }

    public void SetConnection(Connection connection)
    {
        this.connection = connection;
        this.connection.OnConnectionDrooped += Connection_OnConnectionDrooped;
    }

    private void Connection_OnConnectionDrooped(string message)
    {
     //   Debug.Log(message);
    }

    public GameProtocol gameProtocol;

    public static void Create()
    {
        GameObject gameObject = new GameObject("Game");
        var script = gameObject.AddComponent<Game>();
       
        script.Init();
    }

    public void Start()
    {
        protocols = new Dictionary<Protocol.ProtocolType, Protocol>();
        protocols.Add(Protocol.ProtocolType.Game, new GameProtocol(connection));
        gameProtocol = new GameProtocol(connection);
    }

    public void Update()
    {
        //if (connection.IsConnected)
        //{
        //    ProtocolReader protocolReader = connection.Receive();

        //    if (protocolReader != null)
        //    {
        //        Protocol.ProtocolType protocolType = (Protocol.ProtocolType)protocolReader.ReadInt32();

        //        Protocol protocol = null;

        //        if (protocols.TryGetValue(protocolType, out protocol))
        //        {
        //            protocol.ParseMessage(protocolReader);
        //        }
        //    }
        //}


        if (connection.IsConnected)
        {
            List<ProtocolReader> list = connection.ReceiveMessages();

            if (list != null)
                foreach (ProtocolReader protocolReader in list)
                {
                    if (protocolReader != null)
                    {
                        Protocol.ProtocolType protocolType = (Protocol.ProtocolType)protocolReader.ReadInt32();

                        Protocol protocol = null;

                        if (protocols.TryGetValue(protocolType, out protocol))
                        {
                            //Debug.Log("Received message for protocol: " + protocolType);
                            protocol.ParseMessage(protocolReader);
                        }
                    }
                }
        }
    }

    public void CreateShip(int id, ProtocolReader pr)
    {
        GameObject root = UnityEngine.Object.Instantiate(Resources.Load("Root")) as GameObject;
     
        root.name = "space_object_" + id;

        Ship ship = new Ship(id, root, Vector3.zero);
       Root rootScript  = root.GetComponent<Root>();
        rootScript.spaceObject = ship;
        ship.objectId = id;
     //   ship.transform = root.transform;



        Quaternion q = pr.ReadQuaterion();
        Vector3 v3 = pr.ReadVector3();
        ship.gameObject.transform.rotation = q;
        ship.gameObject.transform.position = v3;
        ship.prevRotation = q;
        ship.prevPosition = v3;
        ship.nextRotation = q;
        ship.nextPosition = v3;

        GameObject go = UnityEngine.GameObject.Instantiate(Universe.GetUniverse().jumpOutPrefab, ship.gameObject.transform.position, Quaternion.identity);

        objects.Add(ship);

    }

    public static void Destroy()
    {
        if (instance != null)
        {
            UnityEngine.Object.Destroy(instance.gameObject);
        }
    }

    public void Log(string message)
    {
        Debug.Log(message);
    }


    public void Init()
    {
        instance = GameObject.FindObjectOfType<Game>();
     
        GameObject.DontDestroyOnLoad(instance);
    }

    public void Reset()
    {

    }

    private void OnGUI()
    {

        GUILayout.Space(250);
    
        //StringBuilder sb = new StringBuilder();
        //foreach(SpaceObject o in objects)
        //{
        //    sb.Append("ObjectID: " + o.objectId + " Pos: " + o.gameObject.transform.position + " Rot: " + o.gameObject.transform.rotation + " \n");
        //}

        //GUILayout.Box(sb.ToString());
    }


    private void OnApplicationQuit()
    {
        PlayerPrefs.DeleteAll();
        connection.Disconnect("Quit");
    }
}
