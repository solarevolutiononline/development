﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Selector : MonoBehaviour
{
    public GameObject me;
    [System.NonSerialized]
    public SpaceObject currentTraget;
    public static Selector instance;
   
    void Start()
    {
        instance = this;
    }

   public static SpaceObject Target
    {
        get
        {
            if(instance == null)
            {
                return null;
            }

            return instance.currentTraget;
        }
    }

    // Update is called once per frame
    void Update()
    {
       
        if(currentTraget != null)
        {
           // frameTarget.SetActive(true);
            //frameTarget.transform.Find("Health_bar").transform.Find("progress").GetComponent<Image>().fillAmount = currentTraget.health / currentTraget.maxHealth;
            //frameTarget.transform.Find("Health_bar").transform.Find("text").GetComponent<Text>().text = (int) currentTraget.health + "/" + currentTraget.maxHealth;
            //frameTarget.transform.Find("name").GetComponent<Text>().text = "Enemy";
            if (currentTraget.dead)
            {
                currentTraget = null;
            }
        }
        else
        {
         //   frameTarget.SetActive(false);
        }

        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit raycastHit;

            if (Physics.Raycast(ray, out raycastHit, 20000f, 1))
            {
                SpaceObject spaceObject = raycastHit.collider.gameObject.GetComponent<Root>().spaceObject;

                if (spaceObject != null)
                {
                    if(currentTraget != null)
                    {
                        currentTraget.OnDeselectTest();
                    }

                    currentTraget = spaceObject;
                    spaceObject.gameObject = raycastHit.collider.gameObject;
                    spaceObject.OnSelectTest();
                }
            }
        }

        if(Input.GetKeyDown(KeyCode.C))
        {
           if(currentTraget != null)
            {
                currentTraget.OnDeselectTest();
                currentTraget = null;
                Debug.Log("curr");
            }
        }
        
    }

 
}
