﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using UnityEngine;

public class GameProtocol : Protocol
{
    public enum Request
    {
        Play,
        QWASD,
        SetGear
    }


    public enum Reply
    {
        SyncMove,
        ObjectsLeft,
        ObjectLeft
    }

    public GameProtocol(Connection connection)
    {
        base.connection = connection;
        protocolType = ProtocolType.Game;
    }


    public static GameProtocol GetProtocol()
    {
        return Game.GetInstance().gameProtocol;
    }

    public void Play()
    {
        ProtocolWriter protocolWriter = new ProtocolWriter();
        protocolWriter.Write((int)ProtocolType.Game);
        protocolWriter.Write((int)Request.Play);
        connection.SendMessage(protocolWriter);
    }


    public void QWEASD(float pitch, float yaw, float roll)
    {
        ProtocolWriter protocolWriter = new ProtocolWriter();
        protocolWriter.Write((int)ProtocolType.Game);
        protocolWriter.Write((int)Request.QWASD);
        protocolWriter.Write(pitch);
        protocolWriter.Write(yaw);
        protocolWriter.Write(roll);
        connection.SendMessage(protocolWriter);
    }

    public void SetGear(bool flag)
    {
        ProtocolWriter protocolWriter = new ProtocolWriter();
        protocolWriter.Write((int)ProtocolType.Game);
        protocolWriter.Write((int)Request.SetGear);
        protocolWriter.Write(flag);

        connection.SendMessage(protocolWriter);
    }

    public override void ParseMessage(ProtocolReader protocolReader)
    {
        Reply reply = (Reply)protocolReader.ReadInt32();
   //    Debug.Log(reply);
       
        switch (reply)
        {
            case Reply.SyncMove:

                int id = protocolReader.ReadInt32();

            //    Debug.Log("sync move id: " + id);

                SpaceObject ob = Game.GetInstance().SpaceObjects.Find(o => o.objectId == id);

                if (ob == null)
                {
                    Game.GetInstance().CreateShip(id, protocolReader);
                }
                else
                {

                    ob.Read(protocolReader);
                }

                break;
            case Reply.ObjectLeft:
                {
                    int type = protocolReader.ReadInt32();

                    int id2 = protocolReader.ReadInt32();
                    SpaceObject ob2 = Game.GetInstance().SpaceObjects.Find(o => o.objectId == id2);
                    if (ob2 != null)
                    {
                        switch (type)
                        {
                            case 1:
                                ob2.Destroy();
                                break;
                            case 2:
                                ob2.JumpOut();
                                break;
                        }

                        Game.GetInstance().SpaceObjects.Remove(ob2);
                    }

                }
                break;
            case Reply.ObjectsLeft:

                int amount = protocolReader.ReadInt32();

                for (int i = 0; i < amount; i++)
                {
                    int id2 = protocolReader.ReadInt32();
                    SpaceObject ob2 = Game.GetInstance().SpaceObjects.Find(o => o.objectId == id2);

                    if (ob2 != null)
                    {
                        ob2.Destroy();
                        Game.GetInstance().SpaceObjects.Remove(ob2);
                    }
                }

                break;
            default:
                Debug.Log("Unknow reply in GameProtocol: " + reply);
                break;
        }
    }
}