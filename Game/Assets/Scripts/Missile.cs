﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour
{
    public Transform Target;

    public Vector3 Offset;

    public float Speed;

    public GameObject ExplosionPrefab;

    public SpaceObject tdmg;

    public Vector3 l;

    public bool flag;

    public Transform root;

    public Vector3 to;

    public void In()
    {
        to = root.position;
      //  transform.rotation = Quaternion.LookRotation(Target.position - base.transform.position);
    }

    public float startDistance = 0f;

    public bool Pursuit, FreeFlight;
    public bool test;

    public bool distancerequried;

    private void Start()
    {
        Offset = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f));
    }

    private void Update()
    {

        if(flag)
        {

        
        l = transform.position;


            float startDistance = Vector2.Distance(transform.position, Target.position);

            if (startDistance >= MissileLauncherDataProvider.instance.startDistance)
            {
              //  Vector3 forward = transform.position - l;


                Speed = Mathf.Clamp(Speed + Time.deltaTime * MissileLauncherDataProvider.instance.acceleration, 0f, MissileLauncherDataProvider.instance.maxSpeed);

                Quaternion targetRotation = Quaternion.LookRotation((Target.position + Offset - transform.position).normalized, Vector3.up);
                transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, MissileLauncherDataProvider.instance.trunSpeed * Time.deltaTime);

                transform.position += transform.forward * Speed * Time.deltaTime;
            }
            else
            {
                transform.position += transform.forward * MissileLauncherDataProvider.instance.startSpeed * Time.deltaTime;
            
            }
           

            if (Vector3.Distance(Target.transform.position + Offset, transform.position) <= this.Speed * Time.deltaTime / 2f)
            {
                this.Explode();
            }
        }
    }

 
    private void Explode()
    {
        UnityEngine.Object.Destroy(base.gameObject);

        tdmg.health -= Random.Range(34f, 178f);
        if (tdmg.health <= 0)
        {
            if (!tdmg.dead)
            {
                tdmg.Destroy();
            }
            return;
        }
    }
}
