﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class Root : MonoBehaviour
{
    public SpaceObject spaceObject;

    private List<Spot> spots;
    
      public List<Weapon> weapons;

    public List<GameObject> slots;
    public GameObject fxGun;

    public bool isDebug;
    public Vector3 Position
    {
        get
        {
            return transform.position;
        }
    }

    public Root()
    {
        spots = new List<Spot>();
        weapons = new List<Weapon>();
    }


    public bool isDebugFire = false;

    private void Start()
    {
        if (isDebug)
        {
            for (int i = 0; i < slots.Count; i++)
            {
                GameObject g = slots[i];
                if (g != null && g.gameObject.active)
                {
                    CreateSpot(i, slots[i].gameObject.transform.position, slots[i].gameObject.transform.rotation);
                }
            }

            //InitWeapon(0, new Gun(this) { angle = 60f, reloadTime = 0.4f });
            //InitWeapon(1, new Gun(this) { angle = 60f, reloadTime = 0.4f });
            //InitWeapon(2, new Gun(this) { angle = 60f, reloadTime = 0.4f });
            //InitWeapon(3, new Gun(this) { angle = 75f, reloadTime = 0.4f });
            //InitWeapon(4, new Gun(this) { angle = 75f, reloadTime = 0.4f });
            //InitWeapon(5, new Gun(this) { angle = 75f, reloadTime = 0.4f });
            // InitWeapon(6, new Gun(this) { angle = 75f, reloadTime = 0.7f });
          


            InitWeapon(0, new MissileWeapon(spots[0]) { angle = 190f, reloadTime = 1.25f });
            InitWeapon(1, new MissileWeapon(spots[1]) { angle = 190f, reloadTime = 1.25f });
            InitWeapon(2, new MissileWeapon(spots[2]) { angle = 190f, reloadTime = 1.25f });
            InitWeapon(3, new MissileWeapon(spots[3]) { angle = 190f, reloadTime = 1.25f });
            InitWeapon(4, new MissileWeapon(spots[4]) { angle = 190f, reloadTime = 1.25f });
            InitWeapon(5, new MissileWeapon(spots[5]) { angle = 190f, reloadTime = 1.25f });
            InitWeapon(6, new MissileWeapon(spots[6]) { angle = 75f, reloadTime = 1.25f });
            InitWeapon(7, new MissileWeapon(spots[7]) { angle = 75f, reloadTime = 1.25f });
        }
    }

    private void OnGUI()
    {
        //if(target == null)
        //{
        //    return;
        //}

        //StringBuilder stringBuilder = new StringBuilder();



        //foreach (Weapon weapon in weapons)
        //{
        //    Spot spot = spots[weapon.SpotId];


        //    Vector3 dir = target.gameObject.transform.position - spot.Transform.position;

        //    float angle = Vector3.Angle(spot.Transform.forward, dir);

        //    float a = CalcAngle0to360CW2(spot.Transform.forward, target.gameObject.transform.position - spot.Transform.position);
        //    bool fl = ((a <= arc) || arc == 360f);
        //    stringBuilder.Append("Spot.ID: " + weapon.SpotId + " Cooldown: " + weapon.TimeLeft.ToString("0:0.0") + " Angle: " + angle + "  Can cast: " +  fl + " \n");
        //}

        //GUILayout.Box(stringBuilder.ToString());
    }


    public Vector3 currentPos;
    public Vector3 oldPos;
    public Vector3 velocity;
    public float arc;
    public float magniVelocity;

    public void Update()
    {
        spaceObject.Update();

        currentPos = spaceObject.gameObject.transform.position;
        velocity = (currentPos - oldPos) / Time.deltaTime;
        magniVelocity = velocity.magnitude;
        oldPos = currentPos;
    }

    public void Target(int id)
    {
        target = Game.GetInstance().SpaceObjects.Find(x => x.objectId == id);
    }

    private void LateUpdate()
    {
        if (isDebug)
          

            if(Selector.instance.currentTraget != null)
            {
                target = Selector.instance.currentTraget;
            }
            else
            {
                target = null;
            }

        if (isDebugFire && target != null && target.gameObject != null)
        {
            foreach (Weapon weapon in weapons)
            {
                    if (weapon.CanCast)
                    {
                        Spot spot = spots[weapon.SpotId];

                        float a = CalcAngle0to360CW2(spot.Transform.forward, target.gameObject.transform.position - spot.Transform.position);
                        bool fl = ((a <= arc) || arc == 360f);
                        if (fl)
                        {
                            weapon.Shot(target);
                        }
                    }
            }
        }
    }


    private float CalcAngle0to360CW2(Vector3 vector1, Vector3 vector2)
    {
        float num = Vector3.Angle(vector1, vector2);

        if (arc < 180)
        {
            return num;
        }
        else
        {
            if (Vector3.Cross(vector1, vector2).y > 0f)
            {
                return num;
            }

            return 360;
        }
    }

   private SpaceObject target;

    public void InitWeapon(int spotId, Weapon weapon)
    {
        Weapon foundWeapon = weapons.Find(weapon2 => weapon2.SpotId == spotId);

        if (foundWeapon != null)
        {
            return;
        }

        Spot spot = GetSpot(spotId);

        if (spot != null)
        {
            weapon.SetSpot(spot);
            weapons.Add(weapon);
        }
    }

    public Spot GetSpot(int id)
    {
        return spots.Find(spot => spot.Id == id);
    }

    public void CreateSpot(int id, Vector3 localPosition, Quaternion localRotation)
    {
        Spot foundSpot = spots.Find(spot => spot.Id == id);

        if (foundSpot != null)
        {
            return;
        }

        Spot newSpot = new Spot(id, localPosition, localRotation);
        newSpot.Initialize(this);

        spots.Add(newSpot);
    }


}