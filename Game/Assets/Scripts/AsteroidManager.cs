﻿using UnityEngine;
using System.Collections.Generic;

public class AsteroidManager : MonoBehaviour
{
    private List<GameObject> asteroids;

    public GameObject asteroidPrefab;

    public float radius = 2f;

    public Vector3 v3;

    public int asteroidCount = 50;

    public void Start()
    {
        asteroids = new List<GameObject>();
    }

    private void OnGUI()
    {
        if (GUILayout.Button("Add asteroids"))
        {
            foreach (GameObject g in asteroids)
            {
                GameObject.Destroy(g);
            }

            asteroids.Clear();

            Vector3 vector3 = Vector3.zero; ;

            for (int i = 0; i < asteroidCount; i++)
            {
                float angle = 360f / asteroidCount * i;

                vector3.y += i + radius * Mathf.Sin(6.28318548f * angle);
                vector3.z += i + radius * Mathf.Cos(6.28318548f * angle);
                vector3 += new Vector3(Random.Range(-v3.x, v3.x), Random.Range(-v3.y, v3.y), Random.Range(-v3.z, v3.z));
                GameObject asteroid = GameObject.Instantiate(asteroidPrefab, vector3, Quaternion.identity);
                asteroids.Add(asteroid);
            }
        }

        if (GUILayout.Button("Test"))
        {
            foreach (GameObject g in asteroids)
            {
                GameObject.Destroy(g);
            }

            asteroids.Clear();
            float magnitude = transform.position.magnitude;
            float num = magnitude / this.TwistPeriods;
            float num4;
            for (float num2 = 0f; num2 < magnitude; num2 += num / (6.28318548f * num4 / this.AsteroidDistance))
            {
                float num3 = num2 / magnitude;
                num4 = this.TwistAngle + (1f - this.TwistAngle) * num3;
                Vector3 vector = new Vector3(num2, 0f, 0f);
                vector.y += (this.TwistAngle + (1f - this.TwistAngle) * num3) * this.TwistYRadius * Mathf.Sin(6.28318548f * num3 * this.TwistPeriods);
                vector.z += (this.TwistAngle + (1f - this.TwistAngle) * num3) * this.TwistZRadius * Mathf.Cos(6.28318548f * num3 * this.TwistPeriods);
                vector += new Vector3(UnityEngine.Random.Range(-this.AsteroidRandomPosition, this.AsteroidRandomPosition), UnityEngine.Random.Range(-this.AsteroidRandomPosition, this.AsteroidRandomPosition), UnityEngine.Random.Range(-this.AsteroidRandomPosition, this.AsteroidRandomPosition));
                GameObject asteroid = GameObject.Instantiate(asteroidPrefab, vector, Quaternion.identity);
                asteroids.Add(asteroid);
            }
        }
    }

    public float TwistYRadius = 1500f;


    public float TwistZRadius = 1500f;


    public float TwistPeriods = 1f;


    public float TwistAngle = 1f;


    public float AsteroidRandomPosition;


    public string AsteroidFamily = "asteroid1";


    public float RotationSpeed = 3f;


    public bool RadiusRandom = true;


    public float Radius = 10f;


    public float RadiusMin = 3f;


    public float RadiusMax = 10f;


    public float AsteroidDistance = 0.1f;


    public readonly int MaxAsteriodCount = 250;

}
