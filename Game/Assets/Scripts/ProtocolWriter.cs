﻿using System.IO;
using UnityEngine;

public class ProtocolWriter : BinaryWriter
{
    private MemoryStream memoryStream;

    public ProtocolWriter() : base (new MemoryStream())
    {
        this.memoryStream = (MemoryStream)BaseStream;
    }

    public void Write(Quaternion q)
    {
        Write(q.x);
        Write(q.y);
        Write(q.z);
        Write(q.w);
    }

    public byte[] GetBuffer()
    {
        return memoryStream.GetBuffer();
    }

    public int GetLenght()
    {
        return (int)memoryStream.Length;
    }
}
