﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempCameraScript : MonoBehaviour
{
    public float distance;
    public float startSpeedRot;
    public float startSpeed;
     
    public GameObject ship;
    public float slerpRot;
    public float posLerp;
    private float startFOV;
    public Vector3 offSet;

    public static TempCameraScript instance;
    void Start()
    {
        instance = this;
        // Application.targetFrameRate = 30;
        // startFOV = Camera.main.fieldOfView;
    }

    private void OnGUI()
    {
        //if (GUILayout.Button("qs -1"))
        //{
        //    QualitySettings.vSyncCount = -1;
        //}


        //if (GUILayout.Button("FPS rate 30"))
        //{
        //    Application.targetFrameRate = 30;
        //}


        //if (GUILayout.Button("FPS rate 60"))
        //{
        //    Application.targetFrameRate = 60;
        //}

        //if (GUILayout.Button("FPS unlimited"))
        //{
        //    Application.targetFrameRate = -1;
        //}

    }

    private void OnApplicationQuit()
    {
        PlayerPrefs.DeleteAll();

    }

    public Transform target;

    public float speedRotate = 100f;

    public float limitLookRotation = 45f;

    public float minZoomDistance = 6f;

    public float maxZoomDistance = 50f;

    private float currentZoomDistance = 10f;

    private Vector3 startPosition;

    private Vector3 endPosition;

    public float speedMoving = 0.05f;

    private float deltaMoving = 1f;

    private float xMaxPower;

    private float xLookVelocity;

    public float yMaxPower;

    private float yLookVelocity;


    public float zoom;

 

    void Update()
    {


        if (Game.GetInstance() != null && ship == null)
        {
            SpaceObject o = Game.GetInstance().SpaceObjects.Find(x => x.objectId == Game.GetInstance().myId);

            if (o != null)
            {
                ship = o.gameObject;
            }
        }

        if (ship != null)
        {

            target = ship.transform;



        }
        else
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            transform.rotation = Quaternion.LookRotation(target.position - transform.position, Vector3.up);

        }

        if (Input.GetKey(KeyCode.Mouse1))
        {
      
            float f = Input.GetAxis("Mouse X") * Time.deltaTime * this.speedRotate;
            if (Mathf.Abs(this.xMaxPower) < Mathf.Abs(f) && Mathf.Abs(f) < 10f)
            {
                this.xMaxPower = f;
            }
            f = Input.GetAxis("Mouse Y") * Time.deltaTime * this.speedRotate;
            if (Mathf.Abs(this.yMaxPower) < Mathf.Abs(f) && Mathf.Abs(f) < 10f)
            {
                this.yMaxPower = f;
            }



        }


        this.xMaxPower = Mathf.SmoothDamp(this.xMaxPower, 0f, ref this.xLookVelocity, 0.3f);
        base.transform.RotateAround(this.target.position, new Vector3(0f, 1f, 0f), this.xMaxPower);
        Vector3 lhs = this.target.position - base.transform.position;
        Vector3 rhs = this.target.position - base.transform.TransformPoint(new Vector3(0f, 1f, 0f));
        Vector3 axis = -Vector3.Cross(lhs, rhs);
        this.yMaxPower = Mathf.SmoothDamp(this.yMaxPower, 0f, ref this.yLookVelocity, 0.3f);
        base.transform.RotateAround(this.target.position, axis, this.yMaxPower);
        Vector3 from = Vector3.Scale(this.target.position - base.transform.position, new Vector3(1f, 0f, 1f));
        Vector3 to = this.target.position - base.transform.position;
        if (Vector3.Angle(from, to) > this.limitLookRotation)
        {
            base.transform.RotateAround(this.target.position, axis, -this.yMaxPower);
        }

    }





    private void LateUpdate()
    {
        if (this.target != null)
        {
            //    Quaternion wantedRotation = ship.transform.rotation;
            //    Quaternion currentRotation = transform.rotation;

            //    currentRotation = Quaternion.Lerp(currentRotation, wantedRotation, Time.deltaTime * 10f);

            //    transform.localRotation = currentRotation;

         //   transform.position = target.forward;
           transform.position = target.position - zoom * transform.forward;
        }
    }
}
