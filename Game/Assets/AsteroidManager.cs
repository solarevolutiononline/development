﻿using UnityEngine;

public class AsteroidManager : MonoBehaviour
{
    public GameObject asteroidPrefab;
    public int count;
    public float radius;
    public float ang;
    private void Update()
    {
        float radZ = asteroidPrefab.transform.position.z * Mathf.Deg2Rad;
        float sinZ = Mathf.Sin(radZ);
        float cosZ = Mathf.Cos(radZ);

       asteroidPrefab.transform.position = new Vector3(0f, asteroidPrefab.transform.position.z + radius * Mathf.Cos(ang * Mathf.Deg2Rad), 0f);

    }
}
