﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipMovement : MonoBehaviour
{
    public Text text;
    public float speed, maxSpeed, acceleration;


    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            speed = Mathf.Clamp(speed + acceleration * Time.deltaTime, 0, maxSpeed);
        }
        else
        {
            speed = Mathf.Clamp(speed - (acceleration / 2f) * Time.deltaTime, 0, maxSpeed);
        }

        text.text = "" + Mathf.Round(speed);

        transform.position += transform.forward * speed * 0.1f * Time.deltaTime;
    }
}
