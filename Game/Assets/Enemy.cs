﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Transform target;

    public Transform currentObtacle;
    public Vector3 direction;
    public float dot;

    public bool hasObstacleToAoid;
    public bool hasObstacleToCheck;

    public float distance;
    public float kiedymauciekac = 0.8f;
    public float rotateSpeed;
    public float speed = 49.5f;

    private void Start()
    {
      
    }

    private Vector3 dir;

    public void Update()
    {
        var sortedObstacles = SpawnTest.instance.objects;
        sortedObstacles = sortedObstacles.FindAll(x => Vector3.Distance(transform.position, x.transform.position) <= distance);
        sortedObstacles = sortedObstacles.OrderBy(x => Vector3.Distance(transform.position, x.transform.position)).ToList();

        foreach (var obstacle in sortedObstacles)
        {
            if (!hasObstacleToCheck && !hasObstacleToAoid)
            {
                currentObtacle = obstacle.transform;
                hasObstacleToCheck = true;

                break;
            }
        }


        if (currentObtacle != null)
        {
             dir = currentObtacle.position - transform.position;
            dir.Normalize();
            dot = Vector3.Dot(dir, transform.forward);


            if (dot >= kiedymauciekac && dot > 0)
            {
             
                if (!hasObstacleToAoid)
                {
                    int random = Random.Range(0, 1);
                    //  direction =  ((random ==0)?-1:1) *Vector3.right;
                    direction += currentObtacle.position.normalized * 1000f;
                    hasObstacleToAoid = true;
                    hasObstacleToCheck = false;
                }
            }

        }

        Debug.DrawLine(transform.position, dir, Color.red);


        if (dot < 0f)
        {
            currentObtacle = null;
            direction = target.position - transform.position;
            direction.Normalize();
            hasObstacleToAoid = false;
            hasObstacleToCheck = false;
        }
        

        transform.position += transform.forward * speed * Time.deltaTime;
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction, Vector3.up), rotateSpeed * Time.deltaTime);
    }
}
