﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;



public class SpawnTest : MonoBehaviour
{
    public List<GameObject> asteroids;
    public List<GameObject> objects = new List<GameObject>();
    public int count;
    public Transform spawnPoint;

    public float radius;

    public static SpawnTest instance;

    public float y;

    private void Start()
    {
        instance = this;
        SpawnTest.instance.CreateSector();

    }


    public void Awake()
    {
     
    }
    public void CreateSector()
    {
        for (int i = 1; i < count + 1; i++)
        {
            Vector3 position = spawnPoint.position;
            position.x += Random.Range(-radius, radius) * Mathf.Sin(360f / count * i * Mathf.Deg2Rad);
            position.z += Random.Range(-radius, radius) * Mathf.Cos(360f / count * i * Mathf.Deg2Rad);
            position.y += Random.Range(-y, y) * Mathf.Cos(360f / count * i * Mathf.Deg2Rad);

            float r = Random.Range(8, 10);
            Vector3 scal = new Vector3(r, r, r);
            GameObject spaceObject = GameObject.Instantiate(asteroids[Random.Range(0, asteroids.Count - 1)], position, Random.rotation);
            spaceObject.transform.localScale = scal;
            spaceObject.name = "Asteroid_" + i;
            objects.Add(spaceObject);
        }
    }


    private void OnGUI()
    {
        if (GUILayout.Button("Add asteroids"))
        {

            for (int i = 1; i < count + 1; i++)
            {
                Vector3 position = spawnPoint.position;
                position.x += Random.Range(-radius, radius) * Mathf.Sin(360f / count * i * Mathf.Deg2Rad);
                position.z += Random.Range(-radius, radius) * Mathf.Cos(360f / count * i * Mathf.Deg2Rad);
                position.y += Random.Range(-y, y) * Mathf.Cos(360f / count * i * Mathf.Deg2Rad);
                GameObject spaceObject = GameObject.Instantiate(asteroids[Random.Range(0, asteroids.Count - 1)], position, Quaternion.identity);
                spaceObject.name = "Asteroid_" + i;
                objects.Add(spaceObject);
            }
        }


        if(GUILayout.Button("Clear"))
        {
            objects.ForEach(x => GameObject.Destroy(x));
            objects.Clear();
        }

        if (GUILayout.Button("Add one"))
        {
            Vector3 position = spawnPoint.position;


            position.x += Random.Range(-radius, radius) * Mathf.Sin(360f /  (objects.Count + 1f) * Mathf.Deg2Rad);
            position.z += Random.Range(-radius, radius) * Mathf.Cos(360f /  (objects.Count + 1f) * Mathf.Deg2Rad);
            position += new Vector3(Random.Range(-5f, 5f), 0f, 0f);
            //   position.y += Random.Range(-radius, radius) * Mathf.Sin(360f / objects.Count * Mathf.Deg2Rad);

            GameObject spaceObject = GameObject.Instantiate(asteroids[Random.Range(0, asteroids.Count - 1)], position, Quaternion.identity);
           // spaceObject.transform.parent = transform;
           
            objects.Add(spaceObject);

        }
    }
}
