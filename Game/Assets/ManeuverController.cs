﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ManeuverController : MonoBehaviour
{

    public Vector3 driftVelocity;
    public float pitch, yaw, roll;
    public float speed;
    public float maxSpeed;
    public float speedAcceleration;
    public float inertiaCompensation = 10f;


    public Vector3 oldPosition;
    public Vector3 currentPosition;

    public  Vector3 oldVelocity;
    private Vector3 currentVelocity;

    public Vector3 eulerSpeed;



    public Vector3 maxTurnAcceleration;
    public Vector3 maxTurnSpeed;

    private Quaternion prevRotation;

    public Vector3 tttt;
    public float p, y, r;
    private void QWEASD()
    {
        if (Input.GetKey(KeyCode.W))
        {
            pitch = 1f;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            pitch = -1f;
        }
        else
        {
            pitch = 0f;
        }


        if (Input.GetKey(KeyCode.A))
        {
            yaw = -1f;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            yaw = 1f;
        }
        else
        {
            yaw = 0f;
        }

        if (Input.GetKey(KeyCode.Q))
        {
            roll = 1f;
        }
        else if (Input.GetKey(KeyCode.E))
        {
            roll = -1f;
        }
        else
        {
            roll = 0f;
        }
    }

    public Vector3 cV (Vector3 start, Vector3 v)
    {
       Vector3 vec = start;
        vec.x = Mathf.Clamp(vec.x, (float)-v.x, (float)v.x);
        vec.y = Mathf.Clamp(vec.y, (float)-v.y, (float)v.y);
        vec.z = Mathf.Clamp(vec.z, (float)-v.z,(float)v.z);

        return vec;
    }
    private void Update()
    {

        InputTest();
        Simulation();
     

       

        oldVelocity = currentVelocity;
        currentVelocity = CalculateNewVelocity();
        prevRotation = transform.rotation;
    
    }


    private void InputTest()
    {
        eulerSpeed.x += Input.GetAxis("Vertical") * maxTurnAcceleration.x * Time.deltaTime;
        eulerSpeed.y += Input.GetAxis("Horizontal") * maxTurnAcceleration.y * Time.deltaTime;
        eulerSpeed.z -= Input.GetAxis("Roll") * maxTurnAcceleration.z * Time.deltaTime;

        QWEASD();

        if (Input.GetKey(KeyCode.Space))
        {
            speed = Mathf.Clamp(speed + speedAcceleration * Time.deltaTime, 0f, maxSpeed);
        }
        else
        {
            speed = Mathf.Clamp(speed - speedAcceleration * Time.deltaTime * 2f, 0f, maxSpeed);
        }

        if (pitch == 0)
        {
            if (eulerSpeed.x > 0)
            {
                eulerSpeed.x = Mathf.Clamp(eulerSpeed.x - maxTurnAcceleration.x * Time.deltaTime * 4f, 0f, eulerSpeed.x);
            }
            else
            {
                eulerSpeed.x = Mathf.Clamp(eulerSpeed.x + maxTurnAcceleration.x * Time.deltaTime * 4f, eulerSpeed.x, 0f);
            }
        }

        if (yaw == 0)
        {
            if (eulerSpeed.y > 0)
            {
                eulerSpeed.y = Mathf.Clamp(eulerSpeed.y - maxTurnAcceleration.y * Time.deltaTime * 4f, 0f, eulerSpeed.y);
            }
            else
            {
                eulerSpeed.y = Mathf.Clamp(eulerSpeed.y + maxTurnAcceleration.y * Time.deltaTime * 4f, eulerSpeed.y, 0f);
            }
        }

        if (roll == 0)
        {
            if (eulerSpeed.z > 0)
            {
                eulerSpeed.z = Mathf.Clamp(eulerSpeed.z - maxTurnAcceleration.z * Time.deltaTime * 4f, 0f, eulerSpeed.z);
            }
            else
            {
                eulerSpeed.z = Mathf.Clamp(eulerSpeed.z + maxTurnAcceleration.z * Time.deltaTime * 4f, eulerSpeed.z, 0f);
            }
        }


        if (pitch > 0)
        {
            if (eulerSpeed.x < 0)
            {
                eulerSpeed.x = 0;
            }

        }
        else if (pitch < 0)
        {
            if (eulerSpeed.x > 0)
            {
                eulerSpeed.x = 0;
            }

        }
        if (yaw > 0)
        {
            if (eulerSpeed.y < 0)
            {
                eulerSpeed.y = 0;
            }

        }
        else if (yaw < 0)
        {
            if (eulerSpeed.y > 0)
            {
                eulerSpeed.y = 0;
            }

        }
        if (roll > 0)
        {
            if (eulerSpeed.z < 0)
            {
                eulerSpeed.z = 0;
            }

        }
        else if (roll < 0)
        {
            if (eulerSpeed.z > 0)
            {
                eulerSpeed.z = 0;
            }

        }

        eulerSpeed = cV(eulerSpeed, maxTurnSpeed);

    }

    public void Simulation()
    {
        Vector3 vector = new Vector3(pitch, yaw, roll);
        Vector3 b = maxTurnAcceleration * 0.1f;
        vector = Vector3.Scale(vector, b);
        Vector3 vector2 = eulerSpeed; ;
        bool flag = true;
        if (flag)
        {
            Vector3 vector3 = Quaternion.Inverse(transform.rotation) * vector2;
            if (vector.x == 0f)
            {
                vector.x = SlowingThrust(vector3.x, b.x);
            }
            if (vector.y == 0f)
            {
                vector.y = SlowingThrust(vector3.y, b.y);
            }
            if (vector.z == 0f)
            {
                vector.z = SlowingThrust(vector3.z, b.z);
            }
        }
        Vector3 b2 = transform.rotation * vector;
        Vector3 v = vector2 + b2;
        vector2 = ClampToRotatedBox(v, maxTurnSpeed, transform.rotation);

        tttt = vector2;

     //   transform.rotation =RotateOverTime(transform.rotation, tttt, Time.deltaTime);
        transform.position = transform.position + currentVelocity * Time.deltaTime; //transform.position+ transform.forward * speed * Time.deltaTime; // transform.position + currentVelocity * Time.deltaTime;

          transform.rotation *= Quaternion.Euler(tttt * Time.deltaTime);

     //   transform.rotation = Quaternion.Euler(transform.eulerAngles + tttt * Time.deltaTime);
    }



    public  Quaternion RotateOverTime(Quaternion start, Vector3 changePerSecond, float dt)
    {
        Vector3 vector = changePerSecond;
        Quaternion lhs = Quaternion.AngleAxis(vector.magnitude * dt, vector.normalized);
        return Quaternion.Euler(Rotation(start * lhs));
    }

    public Quaternion RotateOverTimeLocal(Quaternion start, Quaternion changePerSecond, float dt)
    {
        Quaternion rhs = Quaternion.Slerp(Quaternion.identity, changePerSecond, dt);
        return Quaternion.Euler(Rotation(start * rhs));
    }




    public Vector3 Rotation(Quaternion quat)
    {
        float num = quat.x * quat.x;
        float num2 = quat.y * quat.y;
        float num3 = quat.z * quat.z;
        float num4 = quat.w * quat.w;
        float num5 = num + num2 + num3 + num4;
        float num6 = -quat.z * quat.y + quat.x * quat.w;
        float num7;
        float num8;
        float num9;
        if ((double)num6 > 0.499999 * (double)num5)
        {
            num7 = 1.57079637f;
            num8 = 2f * Mathf.Atan2(-quat.z, quat.w);
            num9 = 0f;
        }
        else if ((double)num6 < -0.499999 * (double)num5)
        {
            num7 = -1.57079637f;
            num8 = -2f * Mathf.Atan2(-quat.z, quat.w);
            num9 = 0f;
        }
        else
        {
            num7 = Mathf.Asin(2f * num6 / num5);
            num8 = Mathf.Atan2(2f * quat.y * quat.w + 2f * quat.z * quat.x, -num - num2 + num3 + num4);
            num9 = Mathf.Atan2(2f * quat.z * quat.w + 2f * quat.y * quat.x, -num + num2 - num3 + num4);
        }
        return new Vector3(num7, num8, num9) * 57.29578f;
    }


    private static Vector3 Clamp(Vector3 val, Vector3 min, Vector3 max)
    {
        return Vector3.Min(Vector3.Max(val, min), max);
    }

    public  Vector3 ClampToRotatedBox(Vector3 v, Vector3 halfSideLengths, Quaternion boxOrientation)
    {
        Vector3 val = Quaternion.Inverse(boxOrientation) * v;
        Vector3 point = Clamp(val, -halfSideLengths, halfSideLengths);
        return boxOrientation * point;
    }

    public float SlowingThrust(float v, float maxAccelThisFrame)
    {
        float num = (float)((v >= 0f) ? -1 : 1);
        return num * Mathf.Min(Mathf.Abs(v), Mathf.Abs(maxAccelThisFrame));
    }


    public Vector3 CalculateNewVelocity()
    {
        Vector3 velocity = oldVelocity;
        float driftForce = Vector3.Dot(transform.forward, velocity);
        driftVelocity = velocity - (transform.forward * driftForce);

        Vector3 a = driftVelocity.normalized * GetNewSpeed(driftVelocity.magnitude, 0f, inertiaCompensation);
        Vector3 b = transform.forward * GetNewSpeed(driftForce, speed, speedAcceleration);

        return a + b;

    }


    public static float GetNewSpeed(float current, float target, float acceleration)
    {
        float num = acceleration * 0.1f;
        float num2 = current - target;
        if (Mathf.Abs(num2) < num)
        {
            return target;
        }
        if (num2 > 0f)
        {
            return current - num;
        }
        return current + num;
    }
}


public struct Euler3
{
    public float pitch;

    public float yaw;

    public float roll;

    public static Euler3 zero
    {
        get
        {
            return new Euler3(0f, 0f, 0f);
        }
    }

    public static Euler3 identity
    {
        get
        {
            return Euler3.Rotation(Quaternion.identity);
        }
    }

    public Quaternion rotation
    {
        get
        {
            return Quaternion.Euler(this.pitch, this.yaw, this.roll);
        }
    }

    public Vector3 direction
    {
        get
        {
            return this.rotation * Vector3.forward;
        }
    }

    public Euler3(float pitch, float yaw)
    {
        this.pitch = pitch;
        this.yaw = yaw;
        this.roll = 0f;
    }

    public Euler3(float pitch, float yaw, float roll)
    {
        this.pitch = pitch;
        this.yaw = yaw;
        this.roll = roll;
    }

    public Euler3 Normalized(bool forceStraight)
    {
        float num = Euler3.NormAngle(this.pitch);
        float num2 = this.yaw;
        float num3 = this.roll;
        if (forceStraight && (double)Mathf.Abs(num) > 90.0)
        {
            num = Euler3.NormAngle(179.99f - num);
            num2 += 179.99f;
            num3 += 179.99f;
        }
        return new Euler3(num, Euler3.NormAngle(num2), Euler3.NormAngle(num3));
    }

    private static float NormAngle(float angle)
    {
        while (angle <= -180f)
        {
            angle += 360f;
        }
        while (angle > 180f)
        {
            angle -= 360f;
        }
        return angle;
    }

    public static Euler3 Direction(Vector3 direction)
    {
        float num = Mathf.Atan2(direction.x, direction.z) * 57.29578f;
        float num2 = -Mathf.Atan2(direction.y, Mathf.Sqrt(direction.x * direction.x + direction.z * direction.z)) * 57.29578f;
        return new Euler3(num2, num, 0f);
    }

    public static Euler3 Rotation(Quaternion quat)
    {
        float num = quat.x * quat.x;
        float num2 = quat.y * quat.y;
        float num3 = quat.z * quat.z;
        float num4 = quat.w * quat.w;
        float num5 = num + num2 + num3 + num4;
        float num6 = -quat.z * quat.y + quat.x * quat.w;
        float num7;
        float num8;
        float num9;
        if ((double)num6 > 0.499999 * (double)num5)
        {
            num7 = 1.57079637f;
            num8 = 2f * Mathf.Atan2(-quat.z, quat.w);
            num9 = 0f;
        }
        else if ((double)num6 < -0.499999 * (double)num5)
        {
            num7 = -1.57079637f;
            num8 = -2f * Mathf.Atan2(-quat.z, quat.w);
            num9 = 0f;
        }
        else
        {
            num7 = Mathf.Asin(2f * num6 / num5);
            num8 = Mathf.Atan2(2f * quat.y * quat.w + 2f * quat.z * quat.x, -num - num2 + num3 + num4);
            num9 = Mathf.Atan2(2f * quat.z * quat.w + 2f * quat.y * quat.x, -num + num2 - num3 + num4);
        }
        return new Euler3(num7, num8, num9) * 57.29578f;
    }

    public void Clamp(Euler3 from, Euler3 to)
    {
        this.pitch = Mathf.Clamp(this.pitch, from.pitch, to.pitch);
        this.yaw = Mathf.Clamp(this.yaw, from.yaw, to.yaw);
        this.roll = Mathf.Clamp(this.roll, from.roll, to.roll);
    }

    public void ClampMax(Euler3 max)
    {
        this.pitch = Mathf.Min(this.pitch, max.pitch);
        this.yaw = Mathf.Min(this.yaw, max.yaw);
        this.roll = Mathf.Min(this.roll, max.roll);
    }

    public void ClampMin(Euler3 min)
    {
        this.pitch = Mathf.Max(this.pitch, min.pitch);
        this.yaw = Mathf.Max(this.yaw, min.yaw);
        this.roll = Mathf.Max(this.roll, min.roll);
    }

    public static Euler3 Scale(Euler3 a, Euler3 b)
    {
        return new Euler3(a.pitch * b.pitch, a.yaw * b.yaw, a.roll * b.roll);
    }

    public override string ToString()
    {
        return string.Format("({0},{1},{2})", this.pitch, this.yaw, this.roll);
    }

    public static Euler3 RotateOverTime(Euler3 start, Euler3 changePerSecond, float dt)
    {
        Vector3 vector = changePerSecond.ComponentsToVector3();
        Quaternion lhs = Quaternion.AngleAxis(vector.magnitude * dt, vector.normalized);
        return Euler3.Rotation(lhs * start.rotation);
    }

    public static Euler3 RotateOverTimeLocal(Euler3 start, Euler3 changePerSecond, float dt)
    {
        Quaternion rhs = Quaternion.Slerp(Quaternion.identity, changePerSecond.rotation, dt);
        return Euler3.Rotation(start.rotation * rhs);
    }

    public static Quaternion RotateOverTime(Quaternion start, Quaternion changePerSecond, float dt)
    {
        Quaternion lhs = Quaternion.Slerp(Quaternion.identity, changePerSecond, dt);
        return lhs * start;
    }

    public static Quaternion RotateOverTimeLocal(Quaternion start, Quaternion changePerSecond, float dt)
    {
        Quaternion rhs = Quaternion.Slerp(Quaternion.identity, changePerSecond, dt);
        return start * rhs;
    }

    public Vector3 ComponentsToVector3()
    {
        return new Vector3(this.pitch, this.yaw, this.roll);
    }

    public void ComponentsFromVector3(Vector3 input)
    {
        this.pitch = input.x;
        this.yaw = input.y;
        this.roll = input.z;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override bool Equals(object o)
    {
        return this == (Euler3)o;
    }

    public static bool operator ==(Euler3 a, Euler3 b)
    {
        return a.pitch == b.pitch && a.yaw == b.yaw && a.roll == b.roll;
    }

    public static bool operator !=(Euler3 a, Euler3 b)
    {
        return a.pitch != b.pitch || a.yaw != b.yaw || a.roll != b.roll;
    }

    public static Euler3 operator +(Euler3 a, Euler3 b)
    {
        return new Euler3(a.pitch + b.pitch, a.yaw + b.yaw, a.roll + b.roll);
    }

    public static Euler3 operator -(Euler3 a, Euler3 b)
    {
        return new Euler3(a.pitch - b.pitch, a.yaw - b.yaw, a.roll - b.roll);
    }

    public static Euler3 operator -(Euler3 a)
    {
        return new Euler3(-a.pitch, -a.yaw, -a.roll);
    }

    public static Euler3 operator *(Euler3 a, float b)
    {
        return new Euler3(a.pitch * b, a.yaw * b, a.roll * b);
    }

    public static Euler3 operator /(Euler3 a, float b)
    {
        return new Euler3(a.pitch / b, a.yaw / b, a.roll / b);
    }
}