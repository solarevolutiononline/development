﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test16042019 : MonoBehaviour
{
    // Start is called before the first frame updat

    public Transform target;

    public Vector3 force;

    void Start()
    {
        
    }



    // Update is called once per frame
    void Update()
    {
        float distance = 10f * Time.deltaTime / 2f;

        
        if(Vector3.Distance(transform.position,target.position) <= distance)
        {
            Debug.Log(Vector3.Distance(transform.position, target.position));
            force = transform.position - target.position;
            force.Normalize();
            force *= -10f;
        }

        transform.position += (transform.forward - force) * distance;
    }
}
