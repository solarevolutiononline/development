﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

[System.Serializable]
public class Gun
{
    private List<GunBuff> buffs = new List<GunBuff>();
    private Ammo currentAmmo;
    public float minDamage;
    public float maxDamage;
    public float hitChance;

    public float calculatedMinDamage;
    public float calculatedMaxDamage;
    private float buffMinDamage;
    private float buffMaxDamage;
    private float ammoMinDamage;
    private float ammoMaxDamage;

    [Header("Level")]
    public int level;
    public int maxLevel;
    public float bonus;
    public float procentMinDamage;
    public float ulepszoneGunMinDamage;

    public void AddBuff(GunBuff gunBuff)
    {
        if (gunBuff == null)
        {
            return;
        }

        buffs.Add(gunBuff);
    }

    public void ChooseAmmo(Ammo ammo)
    {
        if (ammo != null)
        {
            currentAmmo = ammo;
        }
    }

    public void Update()
    {
        procentMinDamage = minDamage * bonus / 100f;

        ulepszoneGunMinDamage = minDamage + (procentMinDamage * level / maxLevel);


        buffs.RemoveAll(x => Time.time > x.TimeLeft);
        buffs = buffs.OrderByDescending(x => x.minDamage).ThenBy(x => x.maxDamage).ToList<GunBuff>();

        GunBuff lastBuff = buffs.FirstOrDefault();

        if (lastBuff != null)
        {
            buffMinDamage = minDamage * lastBuff.minDamage * 0.01f;
            buffMaxDamage = maxDamage * lastBuff.maxDamage * 0.01f;
        }
        else
        {
            buffMinDamage = 0f;
            buffMaxDamage = 0f;
        }

        if (currentAmmo != null)
        {
            ammoMinDamage = minDamage * currentAmmo.minDamage * 0.01f;
            ammoMaxDamage = maxDamage * currentAmmo.maxDamage * 0.01f;
        }

        calculatedMinDamage = Math.Max(minDamage + ammoMinDamage + buffMinDamage, 0f);
        calculatedMaxDamage = Math.Max(maxDamage + ammoMaxDamage + buffMaxDamage, 0f);
    }

    public void Shot(Player target)
    {
        if (currentAmmo != null && currentAmmo.amount > 0)
        {
            currentAmmo.amount--;

            float damage = calculatedMaxDamage + (calculatedMinDamage - calculatedMaxDamage) * UnityEngine.Random.Range(0f, 1f);

            bool hit = UnityEngine.Random.Range(0f, 1f) <= hitChance + currentAmmo.accuracy * 0.01f;

            if (hit)
            {
                Debug.Log("damage takaen: " + damage);
                target.TakeDamage(damage);
            }
        }
    }
}
