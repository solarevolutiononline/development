﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class Shop : MonoBehaviour
{
    [Header("Resources")]
    public float money = 1425000f;
    [Header("Item card")]


    public float cenaOd0Do10;
    public float cenaPowyzej10Poziomu;
    public float debugCena;

    public float totalPrice = 142500f;
    public float minPrice;
    public int level = 0;
    public int maxLevel = 15;

    [Header("Var test")]

    public List<float> levelsPrice;
    public int upgradeTo;



    //public float _GetPrice(int level, float maxLevel)
    //{
    //    if (level < 1)
    //        return 0;
    //    //if (level >= maxLevel)
    //    //    return totalPrice;
    //    float a = (minPrice - totalPrice) / (1 - maxLevel);
    //    float b = minPrice - a * 1;
    //    return a * level + b;
    //}

    public float _GetPrice(int level, float maxLevel)
    {
        if (level < 1)
            return 0;
        //if (level >= maxLevel)
        //    return totalPrice;

      
      

        float a = (minPrice - debugCena) / (1 - maxLevel);
        float b = minPrice - a * 1;
        return a * level + b;
    }



    public float procentZCalkowitejCenyOd0do10Poziomu;

    public float GetPrice(float level)
    {

        debugCena = totalPrice; //(float)(totalPrice * (level / (float)maxLevel));

        if (level <= 10)
        {
            debugCena = totalPrice * procentZCalkowitejCenyOd0do10Poziomu * 0.01f;
        }
        else
        {
            debugCena = totalPrice * (float)(totalPrice * (level / (float)maxLevel));
        }

        float iloscPodzialow = (1f + maxLevel) / 2f * maxLevel;

        float obecnyPodzial = (1f + level) / 2f * level;
        podzial = obecnyPodzial;



        return _GetPrice((int)obecnyPodzial, iloscPodzialow);

    }

    public float podzial;
   

    public int GetSuggestedPrice(int price, int maxLevel, int minPrice)
    {
     
        float iloscPodzialow = (1f + maxLevel) / 2f * maxLevel ;
        int min = (int)(price / (iloscPodzialow - 1));
        if (price /(iloscPodzialow - 1) - min >= 0.5f)
            return (min + 1) * (int)(iloscPodzialow - 1) + minPrice;
        else
            return min * (int)(iloscPodzialow - 1) + minPrice;
    }


    

    private void Start()
    {

        totalPrice = GetSuggestedPrice((int)totalPrice, maxLevel,(int) minPrice);
        money = totalPrice;
        //print(GetSuggestedPrice(10000, maxLevel, 10));
        //print(GetSuggestedPrice(1425000, maxLevel, 10));
    }

 
    void Update()
    {
        
        // print(totalPrice * level / (float)maxLevel - totalPrice * (level - 1) / (float)maxLevel);

        //test = totalPrice * (level + 1)/ maxLevel;

        GetPrice(level);
    }

 
    private void OnGUI()
    {
        if(GUILayout.Button("Test"))
        {
            float lvl = 0;
            for (int i = 1; i < maxLevel + 1; i++)
            {
               
                float f = totalPrice * (lvl / (float)maxLevel);
                levelsPrice.Add(f);
                lvl++;
            }

            Debug.Log(levelsPrice.Sum());
        }
       
        if(GUILayout.Button("upgrade"))
        {
            int oldLevel = level;
            level++;
            money -= Mathf.Round(GetPrice(level) - GetPrice(oldLevel));           
        }

        if(GUILayout.Button("Create list (from 0 to " + maxLevel + ")"))
        {
            levelsPrice.Clear();
       
            int oldLevel = 0;

            for (int i = 1; i < maxLevel + 1; i++)
            {
                Debug.Log("Current level: " + i + " Old level: " + oldLevel);
                float t = GetPrice(i) - GetPrice(oldLevel);
                levelsPrice.Add(Mathf.Round(t));
                oldLevel = i;
            }

           

            Debug.Log("Sum: "+ levelsPrice.Sum());
        }

        if(GUILayout.Button("Sort"))
        {
            levelsPrice.Sort();
        }
        //float x = GetPrice(level);
        //float x2 = GetPrice(upgradeTo);

        //if (GUILayout.Button("Upgrade to: " + upgradeTo + " price: "+ (x2-x)))
        //{ 
        //    if(money >= x2 - x)
        //    {
        //        level = upgradeTo;
        //        money -= x2 - x;
        //    }
        //}
    }
}
