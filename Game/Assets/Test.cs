﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    public GameObject target;
    public GameObject missile;
    public float speed;
    public float fireRate;
    public float nextFire;

    void Start()
    {
        
    }

    private void OnGUI()
    {
        GUILayout.Box(": " + target.GetComponent<MeshCollider>().bounds.size);
    }
    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.Mouse0) & Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Debug.Log("Echo");
            GameObject clone = Instantiate(missile, transform.position, Quaternion.identity);
            Missile compoment = clone.AddComponent<Missile>();
            compoment.target = target;
            compoment.speed = speed;
        }
    }
}
