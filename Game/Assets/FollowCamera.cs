﻿using UnityEngine;
using System.Collections;

public class FollowCamera : MonoBehaviour
{
    public Transform target;
    public Vector3 followDistance = new Vector3(0, 2, -10);
    public float positionalDamp = 10;
    public float rotSpeed = 10;

    Transform myTransform;



    void Awake()
    {
        myTransform = transform;

        if (!target) FindTarget();

    }

    public Movement movement;

    void LateUpdate()
    {
        if (!target)
        {
            FindTarget();
            return;
        }

        Vector3 toPos = target.position + (target.rotation * followDistance);
   
        Vector3 curPos = Vector3.Lerp(myTransform.position, toPos, movement.speed * Time.deltaTime);
        myTransform.position = curPos;

        Quaternion q = Quaternion.LookRotation(target.forward, Vector3.up);
        transform.rotation = Quaternion.Lerp(transform.rotation, q, 4.5f * Time.deltaTime);
    }


    void FindTarget()
    {
     //   GameObject temp = GameObject.FindGameObjectWithTag("Player");

      //  if (temp)
         //   target = temp.transform;
    }
}
