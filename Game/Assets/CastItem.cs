﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
public class CastItem : MonoBehaviour
{
    public Button button;
    public Image castBar;
    public bool autoCast;
    public bool cast;
    public bool stillActive;
    public float reloadTime;
    public float timeLeft;
    public float nextCast;
    public float fireRate = 0.5f;
    public float nextFire;
    [Header("Root area")]
    public Player target;
    public Ammo ammo;
    public GunBuff gunBuff;
    public List<Gun> guns = new List<Gun>();
  

    public void Start()
    {
        button.onClick.AddListener(delegate ()
        {
            cast = true;
        });
    } 

    public void Update()
    {
        guns.ForEach(gun => gun.Update());

        if(Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            guns.ForEach(gun => gun.Shot(target));
        }

        if (Time.time > nextCast && (cast || autoCast))
        {
            nextCast = Time.time + reloadTime;

            gunBuff.maxTime = nextCast;
            guns.ForEach(gun =>gun.AddBuff(gunBuff));

            cast = false;
        }
        else
        {
            timeLeft = Mathf.Clamp(nextCast - Time.time, 0f, reloadTime);

            if (timeLeft > 0f)
            {
                stillActive = true;
                castBar.fillAmount = (reloadTime + Time.time - nextCast) / reloadTime;
                castBar.color = new Color(0f, 0f, 0f, ((nextCast - Time.time) / reloadTime) * 0.5f);
            }
            else
            {
                if (stillActive)
                {
                    stillActive = false;
                }
            }
        }
    }

    private void OnGUI()
    {
        if(GUILayout.Button("Choose ammo (defualt)"))
        {
            guns.ForEach(gun => gun.ChooseAmmo(ammo));
        }
    }
}

[System.Serializable]
public class Player
{
    public float MaxHealth;
    public float Health;

    public Player()
    {
        Health = MaxHealth;
    }

    public void TakeDamage(float damage)
    {
        if (Health >= damage)
        {
            Health -= damage;
        }
        else
        {
            Health = MaxHealth;
        }
    }
}

[System.Serializable]
public class Buff
{
    public float maxTime;

    public float TimeLeft
    {
        get
        {
            return maxTime;
        }
    }
}

[System.Serializable]
public class GunBuff : Buff
{
    public float minDamage;
    public float maxDamage;
}

[System.Serializable]
public class Ammo
{
    public float amount;
    public float minDamage;
    public float maxDamage;
    public float accuracy;
}





