﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class Missile : MonoBehaviour
{
    public GameObject target;
    public Vector3 offSet;
    public float speed;

    public void Start()
    {
        var mesh = target.GetComponent<MeshFilter>().mesh;

        var vartieces = mesh.vertices;
        var randomNumber = Random.RandomRange(0, vartieces.Length);

        offSet = vartieces[randomNumber];

        offSet.y *= mesh.bounds.size.y;
        offSet.x *= mesh.bounds.size.z;
        offSet.z = mesh.bounds.size.z;
    }


   

    void Update()
    {
        Vector3 dir = target.transform.position - transform.position;
        dir.Normalize();

        if (Vector3.Distance(target.transform.position, base.transform.position) < speed * Time.deltaTime / 2f)
        {
            Object.Destroy(gameObject);
            Debug.Log("destory");
        }

        transform.LookAt(target.transform.position);
        transform.position += dir * speed * 0.1f * Time.deltaTime;
    }

}
