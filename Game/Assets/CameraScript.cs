﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public Transform PlayerShip;
    public Transform currentTarget;

    public float distance = 3.0f;
    public float height = 3.0f;
    public float damping = 5.0f;
    public bool smoothRotation = true;
    public bool followBehind = true;
    public float rotationDamping = 10.0f;
    public float projectionSphereRadius = 1000f;
    private Quaternion focusError;
    public float focusToTargetStartupTime;


    public float TargetHeightOffset;
    public float ZoomDistance;

    public Quaternion wantedRotation;

    public float rotSpeed;


    public bool adv;
    void Start()
    {
        focusToTargetStartupTime = Time.time;
    }

    private void TargetMode()
    {
        float num = this.TargetHeightOffset * (ZoomDistance / 10f);
        float num2 = ZoomDistance * ZoomDistance - num * num;
        float sqrMagnitude = (currentTarget.position - PlayerShip.position).sqrMagnitude;
        float f = num * num * sqrMagnitude / num2;
        Vector3 b = Mathf.Sqrt(f) * Vector3.up;
        Vector3 forward = currentTarget.position - b - PlayerShip.position;
        if (adv)
        {
            this.wantedRotation = Quaternion.LookRotation(forward, PlayerShip.rotation * Vector3.up);
        }
        else
        {
            this.wantedRotation = Quaternion.LookRotation(forward);
        }
   //     this.rotationSmoothTime = this.TargetDirectionSmoothTime;
    }

    void LateUpdate()
    {
     
 
        TargetMode();
     
        Vector3 a;
        this.CalculateLookAtPoint(out a);


        transform.rotation =  Quaternion.Slerp(transform.rotation, wantedRotation, rotSpeed * Time.deltaTime);
        transform.position = wantedRotation * new Vector3(0f, heightOffset, -ZoomDistance) + PlayerShip.position;


        CamPosition = transform.position;
        CamRotation = transform.rotation;

    }

    public Vector3 test;
    public Vector3 CamPosition;
    public float z, heightOffset;
    public Quaternion CamRotation;

    private Vector3 DetermineCameraUpVector(Transform playerShip, Quaternion camRotation)
    {
     //   return Vector3.Lerp(camRotation * Vector3.up, playerShip.rotation * Vector3.up, 0.5f * Time.deltaTime);
      return playerShip.rotation * Vector3.up;
    }


    public static bool RaySphereIntersection(out Vector3 intersectionPoint, Vector3 rayStart, Vector3 rayDirection, Vector3 sphereCenter, float sphereRadius)
    {
        Vector3 vector = rayStart - sphereCenter;
        float num = 2f * Vector3.Dot(rayDirection, vector);
        float num2 = Vector3.Dot(vector, vector) - sphereRadius * sphereRadius;
        float num3 = num * num - 4f * num2;
        if (num3 > 0f)
        {
            float num4 = (-num - Mathf.Sqrt(num3)) / 2f;
            float num5 = (-num + Mathf.Sqrt(num3)) / 2f;
            if (num4 >= 0f && num5 >= 0f)
            {
                intersectionPoint = rayStart + rayDirection * num4;
                return true;
            }
            if (num4 < 0f && num5 >= 0f)
            {
                intersectionPoint = rayStart + rayDirection * num5;
                return true;
            }
        }
        intersectionPoint = Vector3.zero;
        return false;
    }


    public bool CalculateLookAtPoint(out Vector3 camFocusPoint)
    {
        Vector3 position = PlayerShip.position;
        Quaternion rotation = PlayerShip.rotation;
        Vector3 a = (this.currentTarget != null) ? this.currentTarget.position : (position + rotation * Vector3.forward * 1000f);
        float magnitude = (a - CamPosition).magnitude;
        this.projectionSphereRadius = Mathf.Lerp(this.projectionSphereRadius, magnitude, 5f * Time.deltaTime);
        if ((position - CamPosition).sqrMagnitude > this.projectionSphereRadius * this.projectionSphereRadius)
        {
            this.projectionSphereRadius = (position - CamPosition).magnitude + 0.01f;
        }
        Vector3 vector;
        bool flag = RaySphereIntersection(out vector, position, rotation * Vector3.forward, CamPosition, this.projectionSphereRadius);
        if (flag)
        {
            camFocusPoint = vector;
            return true;
        }
        camFocusPoint = Vector3.zero;
        return false;
    }



}
