﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed;

    public float liveTime;
    // Start is called before the first frame update
    void Start()
    {
        liveTime = 5f + Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time > liveTime)
        {
            UnityEngine.GameObject.Destroy(gameObject);
        }
        transform.position += transform.forward * speed * Time.deltaTime;
    }
}
