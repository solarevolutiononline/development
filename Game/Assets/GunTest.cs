﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunTest : MonoBehaviour
{
    
    private float nextFire;
    public float sleep = 0.4f;
    public GameObject miniGun;
    public ParticleSystem particleSystem;
    public GameObject target;
    public float speed;

    public GameObject slot;

    public float distance;
    public float speedFire;

    public float arcAngle;

    public float a;
    public bool fl;
    public float r;

    public float speedTestx;
    public void OnGUI()
    {
        GUILayout.Box("Angle: " + a);
    }
    void LateUpdate()
    {

        Vector3 dirTemp  = target.gameObject.transform.position -  slot.transform.position;
        dirTemp.Normalize();

         a = Vector3.Angle(slot.transform.forward, dirTemp);

      fl = ((a <= arcAngle * 0.5f) || arcAngle == 360f);

        if (fl)
        {
            // particleSystem.Play();


      //      particleSystem.emissionRate = r;


            particleSystem.enableEmission = true;

        }
        else
        {
            particleSystem.enableEmission = false;
        }


        ParticleSystem.Particle[] p = new ParticleSystem.Particle[particleSystem.particleCount + 1];
        int l = particleSystem.GetParticles(p);



        Vector3 dir = target.transform.position - miniGun.transform.position;
        dir.Normalize();

        miniGun.transform.rotation = Quaternion.LookRotation(dir, Vector3.up);

        int i = 0;
        float delta = 0f;

        distance = (target.gameObject.transform.position - slot.transform.position).magnitude;
        speedFire = distance / speed;

      //  particleSystem.startSpeed = 100f;


        while (i < l)
        {

            var last = p[i].position;
        //    p[i].position = Vector3.Lerp(last, target.transform.position, Time.deltaTime);

         //\\    p[i].velocity += Vector3.Lerp(p[i].velocity, transform.forward, Time.deltaTime);

            i++;
        }

        particleSystem.SetParticles(p, l);
    }

    private float CalcAngle0to360CW2(Vector3 vector1, Vector3 vector2)
    {
        float num = Vector3.Angle(vector1, vector2);

        if (arcAngle < 180)
        {
            return num;
        }
        else
        {
            if (Vector3.Cross(vector1, vector2).y > 0f)
            {
                return num;
            }

            return 360;
        }
    }

}
