﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour
{
    public Transform Target;

    public Vector3 Offset;

    public float Speed;

    public GameObject ExplosionPrefab;



    public Vector3 l;

    public bool flag;

    public void Start()
    {

    }
    private void Update()
    {

        if(flag)
        {

        
        l = transform.position;
        base.transform.position += (this.Target.position + this.Offset - base.transform.position).normalized * this.Speed * 0.1f * Time.deltaTime;
        Vector3 forward = transform.position - l;

        transform.rotation = Quaternion.LookRotation(forward);

            if (Vector3.Distance(this.Target.transform.position + this.Offset, base.transform.position) < this.Speed * Time.deltaTime / 2f)
            {
                this.Explode();
            }
        }
    }

    private void Explode()
    {
        UnityEngine.Object.Destroy(base.gameObject);
        GameObject g = UnityEngine.Object.Instantiate(this.ExplosionPrefab, transform.position, transform.rotation);
        g.AddComponent<FxTest>().target = Target;
 

        
        foreach (Transform t in Target.transform)
        {
            if(t.GetComponent<BoxCollider>() != null)
            t.GetComponent<BoxCollider>().isTrigger = false;
            GameObject.Destroy(t.gameObject, 5f);
            //  Destroy(t.gameObject, 10f);
        }

        Object.Destroy(g, 5f);
    }
}
