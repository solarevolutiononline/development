﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FxTest : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform target;
 
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, target.position, 1f);    
    }
}
