﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RealMovement : MonoBehaviour
{
    Rigidbody rigidBody;
    public List<GameObject> cameras = new List<GameObject>();
    private int currentCamera = 0;

    public float speed;
    public float targetSpeed;
   

    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();

        // theres a weird bug with box colliders affecting max/force - so re-set it
        // http://forum.unity3d.com/threads/adding-a-collider-stops-rotation.42640/
        rigidBody.inertiaTensor = new Vector3(1, 1, 1);
    }
    private float fTargetSpeed = 0.0f;

    private void FixedUpdate()
    {
        rigidBody.AddRelativeTorque(new Vector3(Input.GetAxis("Vertical"), 0, -Input.GetAxis("Horizontal") * 0.5f));


        // thanks to Edy for this gem
        float fForwardSpeed = Vector3.Dot(transform.forward, rigidBody.velocity);


        if (fForwardSpeed > fTargetSpeed)
            rigidBody.AddForce(transform.forward * -5.0f, ForceMode.Force);
        else
            rigidBody.AddForce(transform.forward * 5.0f, ForceMode.Force);


        // ensure we apply forces to make our velocity edge towards our normalized forward vector
        Vector3 vMovement = rigidBody.velocity.normalized;
        Vector3 vFacing = transform.forward;

        Vector3 diff = vFacing - vMovement;
        // apply a gentle push in the opposing direction.
        rigidBody.AddForce(diff * 4.0f);

        speed = ((int)fForwardSpeed);
        targetSpeed = fTargetSpeed;
       // debugText.text = "forward: " + vFacing.ToString() + " vel:" + vMovement.ToString() + " gap:" + diff.ToString();

    }

    private void Update()
    {
        // G for go go go
        if (Input.GetKeyUp(KeyCode.G))
            fTargetSpeed += 5.0f;
        if (Input.GetKeyUp(KeyCode.B))
            fTargetSpeed -= 5.0f;

        if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            cameras[currentCamera].SetActive(false);
            currentCamera++;
            currentCamera = currentCamera % cameras.Count;

            cameras[currentCamera].SetActive(true);
        }
    }
}
