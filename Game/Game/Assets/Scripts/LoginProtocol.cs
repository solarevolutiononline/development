﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;


public class LoginProtocol : Protocol
{
    public enum Request
    {
        Login,
        Logout,
        Play,
        Sync
    }

    public enum Reply
    {
       Init,
       Player,
       Logged,
       Loggedout,
       LoginError,
       Sync
    }

    public enum LoginError
    {
        Succesfull,
        ArleadyConnected,
        Banned,
        UserDoesNotExis,
        Error
    }

    public LoginProtocol()
    {
        protocolType = ProtocolType.Login;
    }

    public ProtocolType ProtocolType
    {
        get
        {
            return protocolType;
        }
    }

    public List<double> times = new List<double>();

    public Stopwatch stopwatch;
    public void Sync()
    {

        stopwatch = new Stopwatch();
        ProtocolWriter protocolWriter = new ProtocolWriter();
        protocolWriter.Write((int)ProtocolType.Login);
        protocolWriter.Write((int)Request.Sync);
        connection.SendMessage(protocolWriter);
        stopwatch.Start();
    }

    public void Login(string username, string password)
    {
        ProtocolWriter protocolWriter = new ProtocolWriter();
        protocolWriter.Write((int)ProtocolType.Login);
        protocolWriter.Write((int)Request.Login);
        protocolWriter.Write(username);
        protocolWriter.Write(password);
        connection.SendMessage(protocolWriter);
    }

    public void Logout()
    {
        ProtocolWriter protocolWriter = new ProtocolWriter();
        protocolWriter.Write((int)ProtocolType.Login);
        protocolWriter.Write((int)Request.Logout);
        connection.SendMessage(protocolWriter);
    }

    public void Play()
    {
        ProtocolWriter protocolWriter = new ProtocolWriter();
        protocolWriter.Write((int)ProtocolType.Login);
        protocolWriter.Write((int)Request.Play);
        connection.SendMessage(protocolWriter);
    }


    public override void ParseMessage(ProtocolReader protocolReader)
    {
        Reply reply = (Reply)protocolReader.ReadInt32();

        switch(reply)
        {
            case Reply.Init:

                Game.Create();
                Game.GetInstance().SetConnection(connection);
              

                break;
            case Reply.Logged:

                int id = protocolReader.ReadInt32();
                string username = protocolReader.ReadString();
                string playerName = protocolReader.ReadString();

                Launcher.GetInstance().SetSession(id, username, playerName);
                Launcher.GetInstance().SetLoginState(true);

                break;
            case Reply.LoginError: //  Launcher.GetInstance().SetLoginState(true);


                LoginError loginResult = (LoginError)protocolReader.ReadInt32();

                Launcher.GetInstance().Show("LoginError: " + loginResult);

                break;
            case Reply.Loggedout:

                Launcher.GetInstance().SetLoginState(false);

                break;
            case Reply.Sync:

              
                break;
        }
    }
}
