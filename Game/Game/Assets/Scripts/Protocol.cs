﻿using UnityEngine;

public class Protocol
{
    public enum ProtocolType
    {
        Login,
        Debug,
        Game
    }

    protected Connection connection;
    protected ProtocolType protocolType;

    public void SetConnection(Connection connection)
    {
        this.connection = connection;
    }

    public virtual void ParseMessage(ProtocolReader protocolReader)
    {

    }
}