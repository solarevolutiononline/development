﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using UnityEngine;

public class GameProtocol : Protocol
{
    public enum Request
    {
        Play,
        QWASD,
        Test,
        AddNpc,
        RemoveNpc,
        KillMe,
        LeaveSector,
        RestartSector
    }


    public enum Reply
    {
        Test,
        SyncMove,
        ObjectLeft,
        ObjectsLeft,
        Reset,
        Id
    }

    public GameProtocol(Connection connection)
    {
        base.connection = connection;
        protocolType = ProtocolType.Game;
    }


    public static GameProtocol GetProtocol()
    {
        return Game.GetInstance().gameProtocol;
    }


    public void AddNpc(int amount)
    {
        ProtocolWriter protocolWriter = new ProtocolWriter();
        protocolWriter.Write((int)ProtocolType.Game);
        protocolWriter.Write((int)Request.AddNpc);
        protocolWriter.Write(amount);
        connection.SendMessage(protocolWriter);
    }

    public void RestartSecrtor()
    {
        ProtocolWriter protocolWriter = new ProtocolWriter();
        protocolWriter.Write((int)ProtocolType.Game);
        protocolWriter.Write((int)Request.RestartSector);
        connection.SendMessage(protocolWriter);
    }


    public void RemoveNpc(int objectId)
    {
        ProtocolWriter protocolWriter = new ProtocolWriter();
        protocolWriter.Write((int)ProtocolType.Game);
        protocolWriter.Write((int)Request.RemoveNpc);
        protocolWriter.Write(objectId);
        connection.SendMessage(protocolWriter);
    }


    public void Hello()
    {
        ProtocolWriter protocolWriter = new ProtocolWriter();
        protocolWriter.Write((int)ProtocolType.Game);
        protocolWriter.Write((int)Request.Test);
 
        connection.SendMessage(protocolWriter);
    }

    public void KillMe()
    {
        ProtocolWriter protocolWriter = new ProtocolWriter();
        protocolWriter.Write((int)ProtocolType.Game);
        protocolWriter.Write((int)Request.KillMe);

        connection.SendMessage(protocolWriter);
    }

    public void Play()
    {
        ProtocolWriter protocolWriter = new ProtocolWriter();
        protocolWriter.Write((int)ProtocolType.Game);
        protocolWriter.Write((int)Request.Play);

        connection.SendMessage(protocolWriter);
    }


    public void SimulationTest()
    {
        ProtocolWriter protocolWriter = new ProtocolWriter();
        protocolWriter.Write((int)ProtocolType.Game);
        protocolWriter.Write((int)Request.QWASD);
        protocolWriter.Write(1f); //x
        protocolWriter.Write(0f); //y
        protocolWriter.Write(1f); //z
        protocolWriter.Write(0f); //w

        protocolWriter.Write(1f); //x
        protocolWriter.Write(0f); //y
        protocolWriter.Write(1f); //z
        connection.SendMessage(protocolWriter);
    }

    public void Simulation(Quaternion q, float speed)
    {
        ProtocolWriter protocolWriter = new ProtocolWriter();
        protocolWriter.Write((int)ProtocolType.Game);
        protocolWriter.Write((int)Request.QWASD);
        protocolWriter.Write(q.x);
        protocolWriter.Write(q.y);
        protocolWriter.Write(q.z);
        protocolWriter.Write(q.w);
        protocolWriter.Write(speed);
        connection.SendMessage(protocolWriter);
    }

    public override void ParseMessage(ProtocolReader protocolReader)
    {
        Reply reply = (Reply)protocolReader.ReadInt32();
       // Debug.Log(reply);

        switch (reply)
        {
            case Reply.Test:
                Game.GetInstance().Log("Hello");
              
                break;
            case Reply.SyncMove:

                int id = protocolReader.ReadInt32();
                
                SpaceObject ob = Game.GetInstance().SpaceObjects.Find(o => o.objectId == id);

                if(ob == null)
                {
                    Game.GetInstance().CreateShip(id, protocolReader); 
                }
                else
                {

                    ob.Read(protocolReader); 
                }

                break;
            case Reply.ObjectLeft:
                {
                    int type = protocolReader.ReadInt32();

                    int id2 = protocolReader.ReadInt32();
                    SpaceObject ob2 = Game.GetInstance().SpaceObjects.Find(o => o.objectId == id2);
                    if(ob2 != null)
                    {
                        switch (type)
                        {
                            case 1:
                                ob2.Destroy();
                                break;
                            case 2:
                                ob2.JumpOut();
                                break;
                        }

                        Game.GetInstance().SpaceObjects.Remove(ob2);
                    }

                }
                break;
            case Reply.ObjectsLeft:

                int amount = protocolReader.ReadInt32();

                for (int i = 0; i < amount; i++)
                {
                    int id2 = protocolReader.ReadInt32();
                    SpaceObject ob2 = Game.GetInstance().SpaceObjects.Find(o => o.objectId == id2);

                    if (ob2 != null)
                    {
                        ob2.Destroy();
                        Game.GetInstance().SpaceObjects.Remove(ob2);
                    }
                }

                break;
            case Reply.Reset:


                
                    int amount2 = protocolReader.ReadInt32();

                    for (int i = 0; i < amount2; i++)
                    {
                        int id2 = protocolReader.ReadInt32();
                        SpaceObject ob2 = Game.GetInstance().SpaceObjects.Find(o => o.objectId == id2);

                        if (ob2 != null)
                        {
                        ob2.JumpOut();
                            Game.GetInstance().SpaceObjects.Remove(ob2);
                        }
                    }
                

                break;
            case Reply.Id:
                Game.GetInstance().myId = protocolReader.ReadInt32();
              
                break;
            default:
                Debug.Log("Unknow reply in GameProtocol: " + reply);
                break;
        }
    }
}