﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Gear
{
    // Token: 0x040026F4 RID: 9972
    None = -1,
    // Token: 0x040026F5 RID: 9973
    Regular,
    // Token: 0x040026F6 RID: 9974
    Boost,
    // Token: 0x040026F7 RID: 9975
    RCS
}

public class Movement : MonoBehaviour
{
    public float x, y, z;
    public float x1, y1, z1;
    public float speed;
   public Text speedText;
    public MovementOptions movementOptions;
    public MovementFrame prevFrame;
    public MovementFrame movementFrame;
  //  public GameObject game;


    public float test;
    //public float drogamagnitude;

    //public float angle;
    //public Vector3 angularSpeed;

    //public float speed;

    //public float debugTest;

    //public Vector3 speedV3;

    public Universe universe;
    public bool q, e, a, d, w, s;

    public void Update()
    {
        x = Input.GetAxis("Vertical");
        y = Input.GetAxis("Horizontal");
        z = -Input.GetAxis("Roll");

        if (x > Mathf.Epsilon)
        {
            x1 = 1;
            prevFrame.Euler3Speed.pitch = Mathf.Clamp(prevFrame.Euler3Speed.pitch + movementOptions.pitchAcceleration * Time.deltaTime, -movementOptions.pitchMaxSpeed, movementOptions.pitchMaxSpeed);

        }
        else if (x < -Mathf.Epsilon)
        {
            x1 = -1;
            prevFrame.Euler3Speed.pitch = Mathf.Clamp(prevFrame.Euler3Speed.pitch - movementOptions.pitchAcceleration * Time.deltaTime, -movementOptions.pitchMaxSpeed, movementOptions.pitchMaxSpeed);
        }
        else
        {
            if (prevFrame.Euler3Speed.pitch < 0)
            {
                prevFrame.Euler3Speed.pitch = Mathf.Clamp(prevFrame.Euler3Speed.pitch + movementOptions.pitchAcceleration * Time.deltaTime, prevFrame.Euler3Speed.pitch, 0f);
            }
            else if (prevFrame.Euler3Speed.pitch> 0)
            {
                prevFrame.Euler3Speed.pitch = prevFrame.Euler3Speed.pitch - movementOptions.pitchAcceleration  * Time.deltaTime;
            }
            else
            {
                prevFrame.Euler3Speed.pitch = 0f;
            }
            x1 = 0;
        }

        if (y > Mathf.Epsilon)
        {
            y1 = 1;
            prevFrame.Euler3Speed.yaw = Mathf.Clamp(prevFrame.Euler3Speed.yaw + movementOptions.yawAcceleration * Time.deltaTime, -movementOptions.yawMaxSpeed, movementOptions.yawMaxSpeed);
        }
        else if (y < -Mathf.Epsilon)
        {
            y1 = -1;
            prevFrame.Euler3Speed.yaw = Mathf.Clamp(prevFrame.Euler3Speed.yaw - movementOptions.yawAcceleration * Time.deltaTime, -movementOptions.yawMaxSpeed, movementOptions.yawMaxSpeed);
        }
        else
        {

            if (prevFrame.Euler3Speed.yaw < 0)
            {
                prevFrame.Euler3Speed.yaw = Mathf.Clamp(prevFrame.Euler3Speed.yaw + movementOptions.yawAcceleration * Time.deltaTime,  prevFrame.Euler3Speed.yaw,0f);
            }
            else if(prevFrame.Euler3Speed.yaw > 0)
            {
                prevFrame.Euler3Speed.yaw = prevFrame.Euler3Speed.yaw - movementOptions.yawAcceleration * Time.deltaTime ;
            }
            else
            {
                prevFrame.Euler3Speed.yaw = 0f;
            }
            y1 = 0;
        }

        if (z > Mathf.Epsilon)
        {
            z1 = 1;
            prevFrame.Euler3Speed.roll = Mathf.Clamp(prevFrame.Euler3Speed.roll + movementOptions.rollAcceleration * Time.deltaTime, -movementOptions.rollMaxSpeed, movementOptions.rollMaxSpeed);
        }
        else if (z < -Mathf.Epsilon)
        {
            z1 = -1;
            prevFrame.Euler3Speed.roll = Mathf.Clamp(prevFrame.Euler3Speed.roll - movementOptions.rollAcceleration * Time.deltaTime, -movementOptions.rollMaxSpeed, movementOptions.rollMaxSpeed);
        }
        else
        {
            if (prevFrame.Euler3Speed.roll < 0)
            {
                prevFrame.Euler3Speed.roll = Mathf.Clamp(prevFrame.Euler3Speed.roll + movementOptions.rollAcceleration * Time.deltaTime , prevFrame.Euler3Speed.roll, 0f);
            }
            else if (prevFrame.Euler3Speed.roll > 0)
            {
                prevFrame.Euler3Speed.roll = prevFrame.Euler3Speed.roll- movementOptions.yawAcceleration * Time.deltaTime;
            }
            else
            {
                prevFrame.Euler3Speed.roll = 0f;
            }

            z1 = 0;
        }

        if (Input.GetKey(KeyCode.Space))
        {
            speed = Mathf.Clamp(speed + movementOptions.acceleration * Time.deltaTime, 0f, movementOptions.speed);
        //    game.SetActive(true);
        }
        else
        {
            speed = Mathf.Clamp(speed -  (movementOptions.acceleration / 2f) * Time.deltaTime, 0f, movementOptions.speed);
         //   game.SetActive(false);
        }

       //  speedText.text = "SPEED: " + Mathf.RoundToInt(speed);

        movementFrame = Simulation.QWEASD(prevFrame, x1, y1, z1, movementOptions);

        //  transform.rotation *= Quaternion.Euler(movementFrame.euler3.pitch, movementFrame.euler3.yaw, movementFrame.euler3.roll);
       
        transform.rotation *= movementFrame.GetFutureRotation(Time.deltaTime * test);
        //  transform.position += transform.forward * speed * test * Time.deltaTime;

        transform.position += transform.forward * speed * test * Time.deltaTime;

        //if(universe.gp != null)
        //universe.gp.Simulation(transform.rotation, speed);


        currentPos = transform.position;
        velocity = (currentPos - prevPos) * Time.deltaTime;

        prevPos = currentPos;


     
      // // velocity.z = 0f;
      //   angleVelocityOffset = Vector3.Angle(transform.forward, velocity);


      //// angleVelocityOffset = 1.0f - 0.9f * Vector3.Angle(transform.forward, prevPos - transform.position) / 180.0f;
       

      //  if (angleVelocityOffset > 45f)
      //  {
      //      movementOptions.speed = Mathf.Lerp(movementOptions.speed, 185f, movementOptions.acceleration * Time.deltaTime);
        
      //  }
      //  else
      //  {
      //      movementOptions.speed = Math.Max(100f, movementOptions.speed - movementOptions.acceleration * Time.deltaTime);
      //  }
    }


    public float angleVelocityOffset;
    public Vector3 currentPos;
    public Vector3 prevPos;
    public Vector3 velocity;

    public static float CorrectRange(float fAngle)
    {
        if (fAngle > 3.14159274f)
        {
            return -6.28318548f + fAngle;
        }
        if (fAngle < -3.14159274f)
        {
            return 6.28318548f + fAngle;
        }
        return fAngle;
    }
    public float angleRot;
}
[System.Serializable]
public struct MovementFrame
{
    // Token: 0x0600315E RID: 12638 RVA: 0x00119884 File Offset: 0x00117A84
    public MovementFrame(Vector3 position, Euler3 euler3, Vector3 linearSpeed, Vector3 strafeSpeed, Euler3 euler3Speed)
    {
        this.position = position;
        this.euler3 = euler3;
        this.linearSpeed = linearSpeed;
        this.strafeSpeed = strafeSpeed;
        this.Euler3Speed = euler3Speed;
        this.mode = 0;
        this.valid = true;
        // this.ActiveThrusterEffects = new List<ThrusterEffect>();
    }

    // Token: 0x170008FD RID: 2301
    // (get) Token: 0x06003160 RID: 12640 RVA: 0x001198DC File Offset: 0x00117ADC
    // (set) Token: 0x06003161 RID: 12641 RVA: 0x001198EC File Offset: 0x00117AEC
    public Quaternion rotation
    {
        get
        {
            return this.euler3.rotation;
        }
        set
        {
            this.euler3 = Euler3.Rotation(value);
        }
    }

    // Token: 0x170008FE RID: 2302
    // (get) Token: 0x06003162 RID: 12642 RVA: 0x001198FC File Offset: 0x00117AFC
    public Vector3 lookDirection
    {
        get
        {
            return this.euler3.direction;
        }
    }

    // Token: 0x170008FF RID: 2303
    // (get) Token: 0x06003163 RID: 12643 RVA: 0x0011990C File Offset: 0x00117B0C
    public Vector3 nextPosition
    {
        get
        {
            return this.position + (this.linearSpeed + this.strafeSpeed) * 0.1f;
        }
    }

    // Token: 0x17000900 RID: 2304
    // (get) Token: 0x06003164 RID: 12644 RVA: 0x00119940 File Offset: 0x00117B40
    public Euler3 NextEuler3
    {
        get
        {
            Euler3 result;
            if (this.mode == 0)
            {
                result = (this.euler3 + this.Euler3Speed * 0.1f).Normalized(true);
            }
            else if (this.mode == 1)
            {
                result = (this.euler3 + this.Euler3Speed * 0.1f).Normalized(false);
            }
            else if (this.mode == 2)
            {
                result = Euler3.RotateOverTime(this.euler3, this.Euler3Speed, 0.1f);
            }
            else if (this.mode == 3)
            {
                result = Euler3.RotateOverTimeLocal(this.euler3, this.Euler3Speed, 0.1f);
            }
            else
            {
                Debug.LogError("MovementFrame.nextEuler: unknown mode " + this.mode);
                result = Euler3.zero;
            }
            return result;
        }
    }

    // Token: 0x06003165 RID: 12645 RVA: 0x00119A2C File Offset: 0x00117C2C
    public Vector3 GetFuturePosition(float t)
    {
        return this.position + (this.linearSpeed + this.strafeSpeed) * t;
    }

    // Token: 0x06003166 RID: 12646 RVA: 0x00119A5C File Offset: 0x00117C5C
    public Quaternion GetFutureRotation(float t)
    {
        if (this.mode == 2)
        {
            return Euler3.RotateOverTime(this.euler3 * 0.1f, this.Euler3Speed * 0.1f , t).rotation;
        }
        if (this.mode == 3)
        {
            return Euler3.RotateOverTimeLocal(this.euler3, this.Euler3Speed, t).rotation;
        }
        return (this.euler3 + this.Euler3Speed * t).rotation;
    }

    // Token: 0x17000901 RID: 2305
    // (get) Token: 0x06003167 RID: 12647 RVA: 0x00119AD8 File Offset: 0x00117CD8
    public static MovementFrame Invalid
    {
        get
        {
            return MovementFrame.invalid;
        }
    }

    // Token: 0x06003168 RID: 12648 RVA: 0x00119AE0 File Offset: 0x00117CE0
    public override string ToString()
    {
        if (this.valid)
        {
            return string.Format("MovementFrame: position = {0}, euler = {1}, linearSpeed = {2}, eulerSpeed = {3}", new object[]
            {
                this.position,
                this.euler3,
                this.linearSpeed,
                this.Euler3Speed
            });
        }
        return "MovemantFrame.Invalid";
    }

    // Token: 0x06003169 RID: 12649 RVA: 0x00119B48 File Offset: 0x00117D48
    private static MovementFrame InvalidFrame()
    {
        return new MovementFrame
        {
            //position = Vector3.zero,
            linearSpeed = Vector3.zero,
            Euler3Speed = Euler3.zero,
            euler3 = Euler3.identity,
            mode = 0,
            valid = false
        };
    }

    // Token: 0x0600316A RID: 12650 RVA: 0x00119BA0 File Offset: 0x00117DA0
    //public void Read(BgoProtocolReader pr)
    //{
    //    this.position = pr.ReadVector3();
    //    this.euler3 = pr.ReadEuler();
    //    this.linearSpeed = pr.ReadVector3();
    //    this.strafeSpeed = pr.ReadVector3();
    //    this.Euler3Speed = pr.ReadEuler();
    //    this.mode = (int)pr.ReadByte();
    //    this.valid = true;
    //}

    // Token: 0x04002763 RID: 10083
    public Vector3 position;

    // Token: 0x04002764 RID: 10084
    public Euler3 euler3;

    // Token: 0x04002765 RID: 10085
    public Vector3 linearSpeed;

    // Token: 0x04002766 RID: 10086
    public Vector3 strafeSpeed;

    // Token: 0x04002767 RID: 10087
    public Euler3 Euler3Speed;

    // Token: 0x04002768 RID: 10088
    // public List<ThrusterEffect> ActiveThrusterEffects;

    // Token: 0x04002769 RID: 10089
    public int mode;

    // Token: 0x0400276A RID: 10090
    public bool valid;

    // Token: 0x0400276B RID: 10091
    private static MovementFrame invalid = MovementFrame.InvalidFrame();
}

[System.Serializable]
public class Simulation
{


    // Use this for initialization
    void Start()
    {
        Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, true);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public static MovementFrame QWEASD(MovementFrame prevFrame, float pitch, float yaw, float roll, MovementOptions options)
    {
        Vector3 vector = new Vector3(pitch, yaw, roll);
        Vector3 b = (options.maxTurnAcceleration * 0.1f).ComponentsToVector3();
        vector = Vector3.Scale(vector, b);
        Vector3 vector2 = prevFrame.Euler3Speed.ComponentsToVector3();
        bool flag = true;
        if (flag)
        {
            Vector3 vector3 = Quaternion.Inverse(prevFrame.rotation) * vector2;
            if (vector.x == 0f)
            {
                vector.x = Simulation.SlowingThrust(vector3.x, b.x);
            }
            if (vector.y == 0f)
            {
                vector.y = Simulation.SlowingThrust(vector3.y, b.y);
            }
            if (vector.z == 0f)
            {
                vector.z = Simulation.SlowingThrust(vector3.z, b.z);
            }
        }
        Vector3 b2 = prevFrame.rotation * vector;
        Vector3 v = vector2 + b2;
        vector2 = Simulation.ClampToRotatedBox(v, options.maxTurnSpeed.ComponentsToVector3(), prevFrame.rotation);
        Euler3 euler3Speed = default(Euler3);
        euler3Speed.ComponentsFromVector3(vector2);
        Vector3 linearSpeed = Simulation.AdvanceLinearSpeed(prevFrame, options);
        Vector3 strafeSpeed = Simulation.AdvanceStrafingSpeed(prevFrame, options, 0f, 0f);
        Euler3 nextEuler = prevFrame.NextEuler3;
        MovementFrame result = new MovementFrame(prevFrame.nextPosition, nextEuler, linearSpeed, strafeSpeed, euler3Speed);
        result.mode = 2;
        Quaternion rotationDelta = nextEuler.rotation * Quaternion.Inverse(prevFrame.rotation) * Quaternion.identity;
        //  result.ActiveThrusterEffects = Simulation.DetermineThrusterEffects(rotationDelta, 0f, 0f);
        return result;
    }

    private static Vector3 Clamp(Vector3 val, Vector3 min, Vector3 max)
    {
        return Vector3.Min(Vector3.Max(val, min), max);
    }

    public static Vector3 ClampToRotatedBox(Vector3 v, Vector3 halfSideLengths, Quaternion boxOrientation)
    {
        Vector3 val = Quaternion.Inverse(boxOrientation) * v;
        Vector3 point = Simulation.Clamp(val, -halfSideLengths, halfSideLengths);
        return boxOrientation * point;
    }

    public static float SlowingThrust(float v, float maxAccelThisFrame)
    {
        float num = (float)((v >= 0f) ? -1 : 1);
        return num * Mathf.Min(Mathf.Abs(v), Mathf.Abs(maxAccelThisFrame));
    }

    public static MovementFrame WASD(MovementFrame prevFrame, int pitch, int yaw, int roll, MovementOptions options)
    {
        Euler3 euler = prevFrame.euler3;
        Euler3 to = options.MaxEulerSpeed(euler);
        Euler3 from = options.MinEulerSpeed(euler);
        float num = 0f;
        if (yaw > 0)
        {
            num = -options.maxRoll;
        }
        if (yaw < 0)
        {
            num = options.maxRoll;
        }
        float num2 = Algorithm3D.NormalizeAngle(num - euler.roll);
        Euler3 zero = Euler3.zero;
        zero.roll = options.rollAcceleration * (num2 / options.maxRoll - prevFrame.Euler3Speed.roll * options.rollFading / options.rollMaxSpeed);
        if (yaw != 0)
        {
            zero.yaw = (float)yaw * options.yawAcceleration;
        }
        else if (prevFrame.Euler3Speed.yaw > Mathf.Epsilon)
        {
            zero.yaw = -options.yawAcceleration * options.yawFading;
            from.yaw = 0f;
        }
        else if (prevFrame.Euler3Speed.yaw < -Mathf.Epsilon)
        {
            zero.yaw = options.yawAcceleration * options.yawFading;
            to.yaw = 0f;
        }
        if (pitch != 0)
        {
            zero.pitch = (float)pitch * options.pitchAcceleration;
        }
        else if (prevFrame.Euler3Speed.pitch > Mathf.Epsilon)
        {
            zero.pitch = -options.pitchAcceleration * options.pitchFading;
            from.pitch = 0f;
        }
        else if (prevFrame.Euler3Speed.pitch < -Mathf.Epsilon)
        {
            zero.pitch = options.pitchAcceleration * options.pitchFading;
            to.pitch = 0f;
        }
        Euler3 euler3Speed = prevFrame.Euler3Speed + zero * 0.1f;
        euler3Speed.Clamp(from, to);
        Vector3 linearSpeed = Simulation.AdvanceLinearSpeed(prevFrame, options);
        Vector3 strafeSpeed = Simulation.AdvanceStrafingSpeed(prevFrame, options, 0f, 0f);
        Euler3 nextEuler = prevFrame.NextEuler3;
        MovementFrame result = new MovementFrame(prevFrame.nextPosition, nextEuler, linearSpeed, strafeSpeed, euler3Speed);
        Quaternion rotationDelta = nextEuler.rotation * Quaternion.Inverse(prevFrame.rotation) * Quaternion.identity;
        // result.ActiveThrusterEffects = Simulation.DetermineThrusterEffects(rotationDelta, 0f, 0f);
        if (pitch != 0)
        {
            result.mode = 1;
        }
        return result;
    }

    public static Vector3 AdvanceLinearSpeed(MovementFrame prevFrame, MovementOptions options)
    {
        Vector3 linearSpeed = prevFrame.linearSpeed;
        Vector3 result = linearSpeed;
        if (options.gear != Gear.RCS)
        {
            Vector3 direction = prevFrame.euler3.direction;
            float num = Vector3.Dot(direction, linearSpeed);
            Vector3 b = direction * num;
            Vector3 vector = linearSpeed - b;
            float magnitude = vector.magnitude;
            Vector3 a = vector.normalized * Simulation.GetNewSpeed(magnitude, 0f, options.inertiaCompensation);
            Vector3 b2 = direction * Simulation.GetNewSpeed(num, options.speed, options.acceleration);
            result = a + b2;
        }
        return result;
    }

    // Token: 0x060031FD RID: 12797 RVA: 0x0011CE84 File Offset: 0x0011B084
    public static Vector3 AdvanceStrafingSpeed(MovementFrame prevFrame, MovementOptions options, float strafeX, float strafeY)
    {
        Vector3 strafeSpeed = prevFrame.strafeSpeed;
        strafeX = Mathf.Clamp(strafeX, -1f, 1f);
        strafeY = Mathf.Clamp(strafeY, -1f, 1f);
        Vector3 point = Quaternion.Inverse(prevFrame.rotation) * strafeSpeed;
        float num = strafeX * options.strafeMaxSpeed;
        float num2 = strafeY * options.strafeMaxSpeed;
        point.x = Simulation.GetNewSpeed(point.x, num, (float)((point.x * num >= -1f) ? 1 : 2) * options.strafeAcceleration);
        point.y = Simulation.GetNewSpeed(point.y, num2, (float)((point.y * num2 >= -1f) ? 1 : 2) * options.strafeAcceleration);
        point.z = Simulation.GetNewSpeed(point.z, 0f, options.strafeAcceleration);
        return prevFrame.rotation * point;
    }

    public static float GetNewSpeed(float current, float target, float acceleration)
    {
        float num = acceleration * 0.1f;
        float num2 = current - target;
        if (Mathf.Abs(num2) < num)
        {
            return target;
            
        }
        if (num2 > 0f)
        {
            return current - num;
        }

        return current + num;
    }


}

[System.Serializable]
public class MovementOptions
{
    // Token: 0x17000902 RID: 2306
    // (get) Token: 0x0600316C RID: 12652 RVA: 0x00119C3C File Offset: 0x00117E3C
    public Euler3 maxTurnAcceleration
    {
        get
        {
            return new Euler3(this.pitchAcceleration, this.yawAcceleration, this.rollAcceleration);
        }
    }

    // Token: 0x17000903 RID: 2307
    // (get) Token: 0x0600316D RID: 12653 RVA: 0x00119C58 File Offset: 0x00117E58
    public Euler3 maxTurnSpeed
    {
        get
        {
            return new Euler3(this.pitchMaxSpeed, this.yawMaxSpeed, this.rollMaxSpeed);
        }
    }

    // Token: 0x0600316E RID: 12654 RVA: 0x00119C74 File Offset: 0x00117E74
    public Euler3 MinEulerSpeed(Euler3 euler3)
    {
        return new Euler3(this.MinPitchSpeed(euler3.pitch), this.MinYawSpeed(euler3.roll, euler3.pitch), this.MinRollSpeed(euler3.roll));
    }

    // Token: 0x0600316F RID: 12655 RVA: 0x00119CB4 File Offset: 0x00117EB4
    public Euler3 MaxEulerSpeed(Euler3 euler3)
    {
        return new Euler3(this.MaxPitchSpeed(euler3.pitch), this.MaxYawSpeed(euler3.roll, euler3.pitch), this.MaxRollSpeed(euler3.roll));
    }

    // Token: 0x06003170 RID: 12656 RVA: 0x00119CF4 File Offset: 0x00117EF4
    public float MinYawSpeed(float roll, float pitch)
    {
        float num = -Mathf.Clamp(Algorithm3D.NormalizeAngle(roll) / this.maxRoll, -1f, 1f);
        float num2 = Mathf.Lerp(-this.yawMaxSpeed, -this.minYawSpeed, (num + 1f) / 2f);
        return num2 * (1f + Mathf.Abs(Algorithm3D.NormalizeAngle(pitch)) * 1f / 90f);
    }

    // Token: 0x06003171 RID: 12657 RVA: 0x00119D64 File Offset: 0x00117F64
    public float MaxYawSpeed(float roll, float pitch)
    {
        float num = -Mathf.Clamp(Algorithm3D.NormalizeAngle(roll) / this.maxRoll, -1f, 1f);
        float num2 = Mathf.Lerp(this.minYawSpeed, this.yawMaxSpeed, (num + 1f) / 2f);
        return num2 * (1f + Mathf.Abs(Algorithm3D.NormalizeAngle(pitch)) * 1f / 90f);
    }

    // Token: 0x06003172 RID: 12658 RVA: 0x00119DD0 File Offset: 0x00117FD0
    public float MinPitchSpeed(float pitch)
    {
        float num = -this.maxPitch * 0.7f;
        if (pitch < num)
        {
            return Mathf.Lerp(-this.pitchMaxSpeed, 0f, Mathf.Clamp01((num - pitch) / (this.maxPitch * 0.3f)));
        }
        return -this.pitchMaxSpeed;
    }

    // Token: 0x06003173 RID: 12659 RVA: 0x00119E20 File Offset: 0x00118020
    public float MaxPitchSpeed(float pitch)
    {
        float num = this.maxPitch * 0.7f;
        if (pitch > num)
        {
            return Mathf.Lerp(this.pitchMaxSpeed, 0f, Mathf.Clamp01((-num + pitch) / (this.maxPitch * 0.3f)));
        }
        return this.pitchMaxSpeed;
    }

    // Token: 0x06003174 RID: 12660 RVA: 0x00119E70 File Offset: 0x00118070
    public float MinRollSpeed(float roll)
    {
        float num = -this.maxRoll * 0.5f;
        return (roll >= num) ? (-this.rollMaxSpeed) : Mathf.Lerp(-this.rollMaxSpeed, 0f, Mathf.Clamp01((num - roll) / (this.maxRoll * 0.5f)));
    }

    // Token: 0x06003175 RID: 12661 RVA: 0x00119EC8 File Offset: 0x001180C8
    public float MaxRollSpeed(float roll)
    {
        float num = this.maxRoll * 0.5f;
        return (roll <= num) ? this.rollMaxSpeed : Mathf.Lerp(this.rollMaxSpeed, 0f, Mathf.Clamp01((-num + roll) / (this.maxRoll * 0.5f)));
    }

    // Token: 0x06003176 RID: 12662 RVA: 0x00119F1C File Offset: 0x0011811C
    //public void Read(BgoProtocolReader pr)
    //{
    //    this.gear = (Gear)pr.ReadByte();
    //    this.speed = pr.ReadSingle();
    //    this.acceleration = pr.ReadSingle();
    //    this.inertiaCompensation = pr.ReadSingle();
    //    this.pitchAcceleration = pr.ReadSingle();
    //    this.pitchMaxSpeed = pr.ReadSingle();
    //    this.yawAcceleration = pr.ReadSingle();
    //    this.yawMaxSpeed = pr.ReadSingle();
    //    this.rollAcceleration = pr.ReadSingle();
    //    this.rollMaxSpeed = pr.ReadSingle();
    //    this.strafeAcceleration = pr.ReadSingle();
    //    this.strafeMaxSpeed = pr.ReadSingle();
    //}

    // Token: 0x06003177 RID: 12663 RVA: 0x00119FBC File Offset: 0x001181BC
    //public void ApplyCard(MovementCard card)
    //{
    //    this.minYawSpeed = card.minYawSpeed * this.yawMaxSpeed;
    //    this.maxPitch = card.maxPitch;
    //    this.maxRoll = card.maxRoll;
    //    this.pitchFading = card.pitchFading;
    //    this.yawFading = card.yawFading;
    //    this.rollFading = card.rollFading;
    //}

    // Token: 0x06003178 RID: 12664 RVA: 0x0011A018 File Offset: 0x00118218
    //public override string ToString()
    //{
    //    return string.Format("Gear: {0}, Speed: {1}, MaxRegularLinSpeed: {2}, Acceleration: {3}, InteriaCompensationti: {4}, PitchAcceleration: {5}, PitchMaxSpeed: {6}, YawAcceleration: {7}, YawMaxSpeed: {8}, RollAcceleration: {9}, RollMaxSpeed: {10}, StrafeAcceleration: {11}, StrafeMaxSpeed: {12}, MinYawSpeed: {13}, MaxPitch: {14}, MaxRoll: {15}, PitchFading: {16}, YawFading: {17}, RollFading: {18}", new object[]
    //    {
    //        this.gear,
    //        this.speed,
    //        this.maxRegularLinSpeed,
    //        this.acceleration,
    //        this.inertiaCompensation,
    //        this.pitchAcceleration,
    //        this.pitchMaxSpeed,
    //        this.yawAcceleration,
    //        this.yawMaxSpeed,
    //        this.rollAcceleration,
    //        this.rollMaxSpeed,
    //        this.strafeAcceleration,
    //        this.strafeMaxSpeed,
    //        this.minYawSpeed,
    //        this.maxPitch,
    //        this.maxRoll,
    //        this.pitchFading,
    //        this.yawFading,
    //        this.rollFading
    //    });
    //}

    // Token: 0x0400276C RID: 10092
    public Gear gear;

    // Token: 0x0400276D RID: 10093
    public float speed;

    // Token: 0x0400276E RID: 10094
    public float maxRegularLinSpeed = 60f;

    // Token: 0x0400276F RID: 10095
    public float acceleration = 10f;

    // Token: 0x04002770 RID: 10096
    public float inertiaCompensation = 10f;

    // Token: 0x04002771 RID: 10097
    public float pitchAcceleration;

    // Token: 0x04002772 RID: 10098
    public float pitchMaxSpeed;

    // Token: 0x04002773 RID: 10099
    public float yawAcceleration;

    // Token: 0x04002774 RID: 10100
    public float yawMaxSpeed;

    // Token: 0x04002775 RID: 10101
    public float rollAcceleration;

    // Token: 0x04002776 RID: 10102
    public float rollMaxSpeed;

    // Token: 0x04002777 RID: 10103
    public float strafeAcceleration = 20f;

    // Token: 0x04002778 RID: 10104
    public float strafeMaxSpeed = 20f;

    // Token: 0x04002779 RID: 10105
    public float minYawSpeed;

    // Token: 0x0400277A RID: 10106
    public float maxPitch;

    // Token: 0x0400277B RID: 10107
    public float maxRoll;

    // Token: 0x0400277C RID: 10108
    public float pitchFading;

    // Token: 0x0400277D RID: 10109
    public float yawFading;

    // Token: 0x0400277E RID: 10110
    public float rollFading;
}

[System.Serializable]
public struct Euler3
{
    // Token: 0x060030C4 RID: 12484 RVA: 0x001177E0 File Offset: 0x001159E0
    public Euler3(float pitch, float yaw)
    {
        this.pitch = pitch;
        this.yaw = yaw;
        this.roll = 0f;
    }

    // Token: 0x060030C5 RID: 12485 RVA: 0x001177FC File Offset: 0x001159FC
    public Euler3(float pitch, float yaw, float roll)
    {
        this.pitch = pitch;
        this.yaw = yaw;
        this.roll = roll;
    }

    // Token: 0x060030C6 RID: 12486 RVA: 0x00117814 File Offset: 0x00115A14
    public Euler3 Normalized(bool forceStraight)
    {
        float num = Euler3.NormAngle(this.pitch);
        float num2 = this.yaw;
        float num3 = this.roll;
        if (forceStraight && (double)Mathf.Abs(num) > 90.0)
        {
            num = Euler3.NormAngle(179.99f - num);
            num2 += 179.99f;
            num3 += 179.99f;
        }
        return new Euler3(num, Euler3.NormAngle(num2), Euler3.NormAngle(num3));
    }

    // Token: 0x170008EC RID: 2284
    // (get) Token: 0x060030C7 RID: 12487 RVA: 0x00117888 File Offset: 0x00115A88
    public static Euler3 zero
    {
        get
        {
            return new Euler3(0f, 0f, 0f);
        }
    }

    // Token: 0x170008ED RID: 2285
    // (get) Token: 0x060030C8 RID: 12488 RVA: 0x001178A0 File Offset: 0x00115AA0
    public static Euler3 identity
    {
        get
        {
            return Euler3.Rotation(Quaternion.identity);
        }
    }

    // Token: 0x060030C9 RID: 12489 RVA: 0x001178AC File Offset: 0x00115AAC
    public static float NormAngle(float angle)
    {
        while (angle <= -180f)
        {
            angle += 360f;
        }
        while (angle > 180f)
        {
            angle -= 360f;
        }
        return angle;
    }

    // Token: 0x060030CA RID: 12490 RVA: 0x001178E4 File Offset: 0x00115AE4
    public static Euler3 Direction(Vector3 direction)
    {
        float num = Mathf.Atan2(direction.x, direction.z) * 57.29578f;
        float num2 = -Mathf.Atan2(direction.y, Mathf.Sqrt(direction.x * direction.x + direction.z * direction.z)) * 57.29578f;
        return new Euler3(num2, num, 0f);
    }

    // Token: 0x060030CB RID: 12491 RVA: 0x00117950 File Offset: 0x00115B50
    public static Euler3 Rotation(Quaternion quat)
    {
        float num = quat.x * quat.x;
        float num2 = quat.y * quat.y;
        float num3 = quat.z * quat.z;
        float num4 = quat.w * quat.w;
        float num5 = num + num2 + num3 + num4;
        float num6 = -quat.z * quat.y + quat.x * quat.w;
        float num7;
        float num8;
        float num9;
        if ((double)num6 > 0.499999 * (double)num5)
        {
            num7 = 1.57079637f;
            num8 = 2f * Mathf.Atan2(-quat.z, quat.w);
            num9 = 0f;
        }
        else if ((double)num6 < -0.499999 * (double)num5)
        {
            num7 = -1.57079637f;
            num8 = -2f * Mathf.Atan2(-quat.z, quat.w);
            num9 = 0f;
        }
        else
        {
            num7 = Mathf.Asin(2f * num6 / num5);
            num8 = Mathf.Atan2(2f * quat.y * quat.w + 2f * quat.z * quat.x, -num - num2 + num3 + num4);
            num9 = Mathf.Atan2(2f * quat.z * quat.w + 2f * quat.y * quat.x, -num + num2 - num3 + num4);
        }
        return new Euler3(num7, num8, num9) * 57.29578f;
    }

    // Token: 0x060030CC RID: 12492 RVA: 0x00117AEC File Offset: 0x00115CEC
    public void Clamp(Euler3 from, Euler3 to)
    {
        this.pitch = Mathf.Clamp(this.pitch, from.pitch, to.pitch);
        this.yaw = Mathf.Clamp(this.yaw, from.yaw, to.yaw);
        this.roll = Mathf.Clamp(this.roll, from.roll, to.roll);
    }

    // Token: 0x060030CD RID: 12493 RVA: 0x00117B58 File Offset: 0x00115D58
    public void ClampMax(Euler3 max)
    {
        this.pitch = Mathf.Min(this.pitch, max.pitch);
        this.yaw = Mathf.Min(this.yaw, max.yaw);
        this.roll = Mathf.Min(this.roll, max.roll);
    }

    // Token: 0x060030CE RID: 12494 RVA: 0x00117BB0 File Offset: 0x00115DB0
    public void ClampMin(Euler3 min)
    {
        this.pitch = Mathf.Max(this.pitch, min.pitch);
        this.yaw = Mathf.Max(this.yaw, min.yaw);
        this.roll = Mathf.Max(this.roll, min.roll);
    }

    // Token: 0x170008EE RID: 2286
    // (get) Token: 0x060030CF RID: 12495 RVA: 0x00117C08 File Offset: 0x00115E08
    public Quaternion rotation
    {
        get
        {
            return Quaternion.Euler(this.pitch, this.yaw, this.roll);
        }
    }

    // Token: 0x170008EF RID: 2287
    // (get) Token: 0x060030D0 RID: 12496 RVA: 0x00117C24 File Offset: 0x00115E24
    public Vector3 direction
    {
        get
        {
            return this.rotation *Vector3.forward;
        }
    }

    // Token: 0x060030D1 RID: 12497 RVA: 0x00117C38 File Offset: 0x00115E38
    public static Euler3 Scale(Euler3 a, Euler3 b)
    {
        return new Euler3(a.pitch * b.pitch, a.yaw * b.yaw, a.roll * b.roll);
    }

    // Token: 0x060030D2 RID: 12498 RVA: 0x00117C78 File Offset: 0x00115E78
    public override string ToString()
    {
        return string.Format("({0},{1},{2})", this.pitch, this.yaw, this.roll);
    }

    // Token: 0x060030D3 RID: 12499 RVA: 0x00117CA8 File Offset: 0x00115EA8
    public static Euler3 RotateOverTime(Euler3 start, Euler3 changePerSecond, float dt)
    {
        Vector3 vector = changePerSecond.ComponentsToVector3();
        Quaternion lhs = Quaternion.AngleAxis(vector.magnitude * dt, vector.normalized);
        return Euler3.Rotation(lhs * start.rotation);
    }

    // Token: 0x060030D4 RID: 12500 RVA: 0x00117CE8 File Offset: 0x00115EE8
    public static Euler3 RotateOverTimeLocal(Euler3 start, Euler3 changePerSecond, float dt)
    {
        Quaternion rhs = Quaternion.Slerp(Quaternion.identity, changePerSecond.rotation, dt);
        return Euler3.Rotation(start.rotation * rhs);
    }

    // Token: 0x060030D5 RID: 12501 RVA: 0x00117D1C File Offset: 0x00115F1C
    public static Quaternion RotateOverTime(Quaternion start, Quaternion changePerSecond, float dt)
    {
        Quaternion lhs = Quaternion.Slerp(Quaternion.identity, changePerSecond, dt);
        return lhs * start;
    }

    // Token: 0x060030D6 RID: 12502 RVA: 0x00117D40 File Offset: 0x00115F40
    public static Quaternion RotateOverTimeLocal(Quaternion start, Quaternion changePerSecond, float dt)
    {
        Quaternion rhs = Quaternion.Slerp(Quaternion.identity, changePerSecond, dt);
        return start * rhs;
    }

    // Token: 0x060030D7 RID: 12503 RVA: 0x00117D64 File Offset: 0x00115F64
    public Vector3 ComponentsToVector3()
    {
        return new Vector3(this.pitch, this.yaw, this.roll);
    }

    // Token: 0x060030D8 RID: 12504 RVA: 0x00117D80 File Offset: 0x00115F80
    public void ComponentsFromVector3(Vector3 input)
    {
        this.pitch = input.x;
        this.yaw = input.y;
        this.roll = input.z;
    }

    // Token: 0x060030D9 RID: 12505 RVA: 0x00117DAC File Offset: 0x00115FAC
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    // Token: 0x060030DA RID: 12506 RVA: 0x00117DC0 File Offset: 0x00115FC0
    public override bool Equals(object o)
    {
        return this == (Euler3)o;
    }

    // Token: 0x060030DB RID: 12507 RVA: 0x00117DD4 File Offset: 0x00115FD4
    public static bool operator ==(Euler3 a, Euler3 b)
    {
        return a.pitch == b.pitch && a.yaw == b.yaw && a.roll == b.roll;
    }

    // Token: 0x060030DC RID: 12508 RVA: 0x00117E10 File Offset: 0x00116010
    public static bool operator !=(Euler3 a, Euler3 b)
    {
        return a.pitch != b.pitch || a.yaw != b.yaw || a.roll != b.roll;
    }

    // Token: 0x060030DD RID: 12509 RVA: 0x00117E5C File Offset: 0x0011605C
    public static Euler3 operator +(Euler3 a, Euler3 b)
    {
        return new Euler3(a.pitch + b.pitch, a.yaw + b.yaw, a.roll + b.roll);
    }

    // Token: 0x060030DE RID: 12510 RVA: 0x00117E9C File Offset: 0x0011609C
    public static Euler3 operator -(Euler3 a, Euler3 b)
    {
        return new Euler3(a.pitch - b.pitch, a.yaw - b.yaw, a.roll - b.roll);
    }

    // Token: 0x060030DF RID: 12511 RVA: 0x00117EDC File Offset: 0x001160DC
    public static Euler3 operator -(Euler3 a)
    {
        return new Euler3(-a.pitch, -a.yaw, -a.roll);
    }

    // Token: 0x060030E0 RID: 12512 RVA: 0x00117EFC File Offset: 0x001160FC
    public static Euler3 operator *(Euler3 a, float b)
    {
        return new Euler3(a.pitch * b, a.yaw * b, a.roll * b);
    }

    // Token: 0x060030E1 RID: 12513 RVA: 0x00117F2C File Offset: 0x0011612C
    public static Euler3 operator /(Euler3 a, float b)
    {
        return new Euler3(a.pitch / b, a.yaw / b, a.roll / b);
    }

    // Token: 0x040026F0 RID: 9968
    public float pitch;

    // Token: 0x040026F1 RID: 9969
    public float yaw;

    // Token: 0x040026F2 RID: 9970
    public float roll;
}
public static class Algorithm3D
{
    // Token: 0x060037C6 RID: 14278 RVA: 0x0013C3D0 File Offset: 0x0013A5D0
    public static Vector3 DistanceToLineSection(Vector3 from, Vector3 lineBegin, Vector3 lineEnd)
    {
        Vector3 vector = lineEnd - lineBegin;
        Vector3 lhs = from - lineBegin;
        float num = Vector3.Dot(lhs, vector);
        if (num <= 0f)
        {
            return lineBegin - from;
        }
        float num2 = Vector3.Dot(vector, vector);
        if (num2 <= num)
        {
            return lineEnd - from;
        }
        float d = num / num2;
        Vector3 a = lineBegin + d * vector;
        return a - from;
    }

    // Token: 0x060037C7 RID: 14279 RVA: 0x0013C43C File Offset: 0x0013A63C
    public static Vector3 CubicHermiteSpline(Vector3 p0, Vector3 m0, Vector3 p1, Vector3 m1, float t)
    {
        float num = t * t;
        float num2 = num * t;
        return (2f * num2 - 3f * num + 1f) * p0 + (num2 - 2f * num + t) * m0 + (-2f * num2 + 3f * num) * p1 + (num2 - num) * m1;
    }

    // Token: 0x060037C8 RID: 14280 RVA: 0x0013C4B0 File Offset: 0x0013A6B0
    public static Vector3 CubicBezierCurves(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
    {
        float num = t * t;
        float d = num * t;
        float num2 = 1f - t;
        float num3 = num2 * num2;
        float d2 = num3 * num2;
        return d2 * p0 + 3f * num3 * t * p1 + 3f * num2 * num * p2 + d * p3;
    }

    // Token: 0x060037C9 RID: 14281 RVA: 0x0013C51C File Offset: 0x0013A71C
    public static Vector3 QuadraticBézierCurves(Vector3 p0, Vector3 p1, Vector3 p2, float t)
    {
        float d = t * t;
        float num = 1f - t;
        float d2 = num * num;
        return d2 * p0 + 2f * num * t * p1 + d * p2;
    }

    // Token: 0x060037CA RID: 14282 RVA: 0x0013C564 File Offset: 0x0013A764
    public static Vector3 CubicBezierCurvesTangent(Vector3 point0, Vector3 m0, Vector3 point1, Vector3 m1, float t)
    {
        Vector3 p = point0 + m0;
        Vector3 p2 = point1 - m1;
        return Algorithm3D.CubicBezierCurves(point0, p, p2, point1, t);
    }

    // Token: 0x060037CB RID: 14283 RVA: 0x0013C590 File Offset: 0x0013A790
    public static Vector3 Clamp(Vector3 value, Vector3 min, Vector3 max)
    {
        return new Vector3
        {
            x = Mathf.Clamp(value.x, min.x, max.x),
            y = Mathf.Clamp(value.y, min.y, max.y),
            z = Mathf.Clamp(value.z, min.z, max.z)
        };
    }

    // Token: 0x060037CC RID: 14284 RVA: 0x0013C60C File Offset: 0x0013A80C
    public static bool IsNaN(Quaternion q)
    {
        return float.IsNaN(q.w) || float.IsNaN(q.x) || float.IsNaN(q.y) || float.IsNaN(q.z);
    }

    // Token: 0x060037CD RID: 14285 RVA: 0x0013C65C File Offset: 0x0013A85C
    public static bool IsInfinity(Quaternion q)
    {
        return float.IsInfinity(q.w) || float.IsInfinity(q.x) || float.IsInfinity(q.y) || float.IsInfinity(q.z);
    }

    // Token: 0x060037CE RID: 14286 RVA: 0x0013C6AC File Offset: 0x0013A8AC
    public static bool Raycast(GameObject gameObject, Vector3 origin, Vector3 direction, float distance, out RaycastHit raycastHit)
    {
        int layer = gameObject.layer;
        gameObject.layer = 4;
        bool result = Physics.Raycast(origin, direction.normalized, out raycastHit, distance, 1);
        gameObject.layer = layer;
        return result;
    }

    // Token: 0x060037CF RID: 14287 RVA: 0x0013C6E4 File Offset: 0x0013A8E4
    public static bool Linecast(GameObject gameObject, Vector3 start, Vector3 end, out RaycastHit raycastHit)
    {
        int layer = gameObject.layer;
        gameObject.layer = 4;
        bool result = Physics.Linecast(start, end, out raycastHit, 1);
        gameObject.layer = layer;
        return result;
    }

    // Token: 0x060037D0 RID: 14288 RVA: 0x0013C714 File Offset: 0x0013A914
    public static Vector3 GetTrueDirection(Vector3 direction)
    {
        Vector3 vector = direction.normalized;
        if (Vector3.Angle(vector, Vector3.up) < 40f)
        {
            Vector3 from = vector;
            from.y = 0f;
            from.Normalize();
            vector = Vector3.Slerp(from, Vector3.up, 0.5555556f);
        }
        else if (Vector3.Angle(vector, Vector3.down) < 40f)
        {
            Vector3 from2 = vector;
            from2.y = 0f;
            from2.Normalize();
            vector = Vector3.Slerp(from2, Vector3.down, 0.5555556f);
        }
        return vector.normalized;
    }

    // Token: 0x060037D1 RID: 14289 RVA: 0x0013C7AC File Offset: 0x0013A9AC
    public static Quaternion RotateQuaternion(Quaternion q1, Quaternion q2, float rotationAngle)
    {
        float num = Quaternion.Angle(q1, q2);
        if (Mathf.Approximately(num, 0f) || num <= rotationAngle)
        {
            return q2;
        }
        return Quaternion.Slerp(q1, q2, rotationAngle / num);
    }

    // Token: 0x060037D2 RID: 14290 RVA: 0x0013C7E4 File Offset: 0x0013A9E4
    public static Quaternion MirrorQuaternion(Quaternion q)
    {
        float num = 0f;
        Vector3 zero = Vector3.zero;
        q.ToAngleAxis(out num, out zero);
        zero.x = -zero.x;
        return Quaternion.AngleAxis(-num, zero);
    }

    // Token: 0x060037D3 RID: 14291 RVA: 0x0013C820 File Offset: 0x0013AA20
    public static Vector3 Distance(Vector3 s1p0, Vector3 s1p1, Vector3 s2p0, Vector3 s2p1)
    {
        Vector3 vector = s1p1 - s1p0;
        Vector3 vector2 = s2p1 - s2p0;
        Vector3 vector3 = s1p0 - s2p0;
        float num = Vector3.Dot(vector, vector);
        float num2 = Vector3.Dot(vector, vector2);
        float num3 = Vector3.Dot(vector2, vector2);
        float num4 = Vector3.Dot(vector, vector3);
        float num5 = Vector3.Dot(vector2, vector3);
        float num6 = num * num3 - num2 * num2;
        float num7 = num6;
        float num8 = num6;
        float num9;
        float num10;
        if (num6 < Mathf.Epsilon)
        {
            num9 = 0f;
            num7 = 1f;
            num10 = num5;
            num8 = num3;
        }
        else
        {
            num9 = num2 * num5 - num3 * num4;
            num10 = num * num5 - num2 * num4;
            if (num9 < 0f)
            {
                num9 = 0f;
                num10 = num5;
                num8 = num3;
            }
            else if (num9 > num7)
            {
                num9 = num7;
                num10 = num5 + num2;
                num8 = num3;
            }
        }
        if (num10 < 0f)
        {
            num10 = 0f;
            if (-num4 < 0f)
            {
                num9 = 0f;
            }
            else if (-num4 > num)
            {
                num9 = num7;
            }
            else
            {
                num9 = -num4;
                num7 = num;
            }
        }
        else if (num10 > num8)
        {
            num10 = num8;
            if (-num4 + num2 < 0f)
            {
                num9 = 0f;
            }
            else if (-num4 + num2 > num)
            {
                num9 = num7;
            }
            else
            {
                num9 = -num4 + num2;
                num7 = num;
            }
        }
        float d = (Mathf.Abs(num9) >= Mathf.Epsilon) ? (num9 / num7) : 0f;
        float d2 = (Mathf.Abs(num10) >= Mathf.Epsilon) ? (num10 / num8) : 0f;
        return vector3 + d * vector - d2 * vector2;
    }

    // Token: 0x060037D4 RID: 14292 RVA: 0x0013C9F4 File Offset: 0x0013ABF4
    public static void Distance(Vector3 s0, Vector3 s1, Vector3 s2, Vector3 s3, out Vector3 p0, out Vector3 p1)
    {
        Vector3 a = s1 - s0;
        Vector3 b = s3 - s2;
        Vector3 rhs = a - b;
        Vector3 lhs = s2 - s0;
        float sqrMagnitude = rhs.sqrMagnitude;
        float num = 0f;
        if (!Mathf.Approximately(sqrMagnitude, 0f))
        {
            num = Vector3.Dot(lhs, rhs) / sqrMagnitude;
        }
        num = Mathf.Clamp01(num);
        p0 = s0 + (s1 - s0) * num;
        p1 = s2 + (s3 - s2) * num;
    }

    // Token: 0x060037D5 RID: 14293 RVA: 0x0013CA90 File Offset: 0x0013AC90
    public static Vector3 NormalizeEuler(Vector3 v)
    {
        return new Vector3(Algorithm3D.NormalizeAngle(v.x), Algorithm3D.NormalizeAngle(v.y), Algorithm3D.NormalizeAngle(v.z));
    }

    // Token: 0x060037D6 RID: 14294 RVA: 0x0013CABC File Offset: 0x0013ACBC
    public static Vector3 NormalizeEuler(Vector3 v, Vector3 nearest)
    {
        return new Vector3(Algorithm3D.NormalizeAngle(v.x, nearest.x), Algorithm3D.NormalizeAngle(v.y, nearest.y), Algorithm3D.NormalizeAngle(v.z, nearest.z));
    }

    // Token: 0x060037D7 RID: 14295 RVA: 0x0013CB08 File Offset: 0x0013AD08
    public static float NormalizeAngle(float angle)
    {
        while (angle <= -180f)
        {
            angle += 360f;
        }
        while (angle > 180f)
        {
            angle -= 360f;
        }
        return angle;
    }

    // Token: 0x060037D8 RID: 14296 RVA: 0x0013CB40 File Offset: 0x0013AD40
    public static float NormalizeAngle(float angle, float nearest)
    {
        while (angle - nearest < 0f)
        {
            angle += 360f;
        }
        while (angle - nearest > 180f)
        {
            angle -= 360f;
        }
        return angle;
    }

    // Token: 0x04002EB5 RID: 11957
    public const int defaultLayerMask = 1;

    // Token: 0x04002EB6 RID: 11958
    public const int ignoreReycastLayerMask = 4;
}




