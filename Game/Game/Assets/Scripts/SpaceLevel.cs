﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceLevel : MonoBehaviour
{
    private void Update()
    {
        if (Game.GetInstance() == null)
            return;

        List<SpaceObject> spaceObjects = Game.GetInstance().SpaceObjects;

        for (int i = 0; i < spaceObjects.Count; i++)
        {
            SpaceObject spaceObject = spaceObjects[i];
            spaceObject.Update();
        }
    }
}
