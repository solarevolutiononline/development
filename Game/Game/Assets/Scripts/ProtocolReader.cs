﻿using System.IO;
using UnityEngine;

public class ProtocolReader : BinaryReader
{
    public ProtocolReader(byte[] buffer) : base(new MemoryStream(buffer))
    {

    }

    public ProtocolReader(byte[] buffer, int index, int count) : base(new MemoryStream(buffer, index, count))
    {

    }

    public ProtocolReader(MemoryStream stream) : base(stream)
    {

    }

    public Vector3 ReadVector3()
    {
        return new Vector3(base.ReadSingle(), base.ReadSingle(), base.ReadSingle());
    }

    public Quaternion ReadQuaterion()
    {
        return new Quaternion(base.ReadSingle(), base.ReadSingle(), base.ReadSingle(), base.ReadSingle());
    }
}
