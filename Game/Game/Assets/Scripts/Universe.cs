﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Universe : MonoBehaviour
{
    private Connection connection;
    public GameProtocol gp;

    public GameObject explosionPrefab;
    public GameObject jumpOutPrefab;

    private static Universe instance;
    public static Universe GetUniverse()
    {
        return instance;
    }
    public int objectId;
 //   public Quaternion q;
    private void Start()
    {
        instance = this;
        Application.targetFrameRate = 30;

        connection = new Connection();
        connection.Connect("127.0.0.1", 27050);
        connection.isLive = false;

        Game.Create();
        Game.GetInstance().SetConnection(connection);

        gp = new GameProtocol(connection);
        gp.Play();
    }

    private void Update()
    {
      // gp.Simulation(q);
    }

    private void OnGUI()
    { 
        //if(GUILayout.Button("Respawn me"))
        //{
        //    gp.Play();
        //}

        //if (GUILayout.Button("Kill me"))
        //{
        //    gp.KillMe();
        //}

        //if(GUILayout.Button("Add enemy x1"))
        //{
        //    gp.AddNpc(1);
        //}


        //if (GUILayout.Button("Add enemy x10"))
        //{
        //    gp.AddNpc(10);
        //}

        //if (GUILayout.Button("Add enemy x100"))
        //{
        //    gp.AddNpc(100);
        //}

        //if (GUILayout.Button("qs -1"))
        //{
        //    QualitySettings.vSyncCount = -1;
        //}


        //if (GUILayout.Button("FPS rate 30"))
        //{
        //    Application.targetFrameRate = 30;
        //}


        //if (GUILayout.Button("FPS rate 60"))
        //{
        //    Application.targetFrameRate = 60;
        //}

        //if (GUILayout.Button("FPS unlimited"))
        //{
        //    Application.targetFrameRate = -1;
        //}


        //if(GUILayout.Button("Connect to localhost"))
        //{
        //    connection.Connect("127.0.0.1",27050);
        //}

        //if (GUILayout.Button("qs 0"))
        //{
        //    QualitySettings.vSyncCount = 0;
        //}

        //if (GUILayout.Button("qs 1"))
        //{
        //    QualitySettings.vSyncCount = 1;
        //}

        //if (GUILayout.Button("qs 2"))
        //{
        //    QualitySettings.vSyncCount = 2;
        //}

        //if (GUILayout.Button("qs 3"))
        //{
        //    QualitySettings.vSyncCount = 3;
        //}


        //if (GUILayout.Button("Add enemy (20)"))
        //{
        //    gp.AddNpc(20);
        //}


        //if (GUILayout.Button("Add enemy"))
        //{
        //    gp.AddNpc(1);
        //}

        //if (GUILayout.Button("Remove Object"))
        //{
        //    gp.RemoveNpc(objectId);
        //}


        //if(GUILayout.Button("Simulation test"))
        //{
        //    gp.SimulationTest();
        //}
    }
}
