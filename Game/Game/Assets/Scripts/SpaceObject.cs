﻿using UnityEngine;

[System.Serializable]
public class SpaceObject
{
    public float overTime;
    public int objectId;
    public GameObject gameObject;
    public Vector3 nextPosition;
    public Vector3 prevPosition;
    public Vector3 realPosition;

    public  Quaternion prevRotation;
    public Quaternion nextRotation;

    public float framePos, frameRot;
   
    public float lastMove;
   
    public float updateDelta, prevUpdateDelta;
    public float delta;
    public float timeSinceLastUpdate = 0;
    public Vector3 v3;

    public Quaternion q;

    public Vector3 eulerSpeed, lastEuler;

    public bool local;

    public float t;
    public void Destroy()
    {
        GameObject go = UnityEngine.GameObject.Instantiate(Universe.GetUniverse().explosionPrefab, gameObject.transform.position, Quaternion.identity);
        UnityEngine.GameObject.Destroy(gameObject, 0.75f);
        Debug.Log("Destory");
    }

    public void JumpOut()
    {
        GameObject go = UnityEngine.GameObject.Instantiate(Universe.GetUniverse().jumpOutPrefab, gameObject.transform.position, Quaternion.identity);
        UnityEngine.GameObject.Destroy(gameObject, 0.75f);
        Debug.Log("JumpOut");
    }

    public virtual void Update()
    {


    }

    public virtual void Read(ProtocolReader protocolReader)
    {

    }
}

[System.Serializable]
public class Ship : SpaceObject
{

    public float speed;

    public Ship(int id, GameObject gameObject, Vector3 v3)
    {
        this.objectId = id;
        this.gameObject = gameObject;
        this.gameObject.transform.position = v3;
        lastMove = Time.realtimeSinceStartup;
        nextPosition = v3;
    }


    public  virtual void Destory()
    {

        GameObject go = UnityEngine.GameObject.Instantiate(Universe.GetUniverse().explosionPrefab, gameObject.transform);
        UnityEngine.GameObject.Destroy(gameObject);
        Debug.Log("Destroy");
    }

 
    
    public override void Update()
    {
        delta = Time.realtimeSinceStartup - lastMove;

        lastMove = Time.realtimeSinceStartup;

        timeSinceLastUpdate += delta;






        // gameObject.transform.position += gameObject.transform.forward * speed * 0.1f * Time.deltaTime;

       t = timeSinceLastUpdate / updateDelta;
        gameObject.transform.rotation = Quaternion.Lerp(prevRotation, nextRotation, t);
        gameObject.transform.position = Vector3.Lerp(prevPosition, nextPosition, t);

        //  gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, nextPosition, timeSinceLastUpdate / updateDelta);
        // gameObject.transform.rotation = Quaternion.RotateTowards(gameObject.transform.rotation, nextRotation, timeSinceLastUpdate / updateDelta);

        //if (!local)
        //{
        //    gameObject.transform.position = Vector3.Lerp(prevPosition, nextPosition, timeSinceLastUpdate / updateDelta);
        //}
        //else
        //{
        //    gameObject.transform.position += gameObject.transform.rotation * Vector3.forward * 0.1f * speed * timeSinceLastUpdate;

        //if ((prevPosition - gameObject.transform.position).sqrMagnitude > 0.02f || Quaternion.Angle(prevRotation, gameObject.transform.rotation) > 0.02f)
        //{
        //    Debug.Log("lag");
        //    gameObject.transform.position = prevPosition;
        //    gameObject.transform.rotation = prevRotation;
        //}
    }

    public override void Read(ProtocolReader pr)
    {
        if (prevUpdateDelta == 0)
        {
            prevUpdateDelta = Time.realtimeSinceStartup;
        }

        
        updateDelta = Time.realtimeSinceStartup - prevUpdateDelta;
      
        prevUpdateDelta = Time.realtimeSinceStartup;

        prevRotation = nextRotation;
        prevPosition = nextPosition;
     

        nextRotation = pr.ReadQuaterion();
        nextPosition = pr.ReadVector3();

        speed = pr.ReadSingle();

        timeSinceLastUpdate = 0f;
    }
}

//C:\Users\Filip\Desktop\New\Game\Assets