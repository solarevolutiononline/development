﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Globalization;

public class HUD : MonoBehaviour
{
    public Text pingText;
    public Text fpsText;
    public Text bandwidth;

    public float updateInterval = 0.5F;
    private double lastInterval;
    private int frames = 0;
    private float fps;

    public float nextUpdate;

    private uint lastTotalReceivedData, lastTotalSentData;

    private float totalReceivedDataDelta, totalSendDataDelta;

    private uint currentTotalReceivedData, currentTotalSentData;
    public float wait = 10;

    private string GetFormatStr(uint data)
    {
        uint num = data / 1000000u;
        uint num2 = data % 1000000u / 1000u;
        uint num3 = data % 1000u;
        if (num != 0u)
        {
            return string.Format("{0:000} {1:000} {2,000}", num, num2, num3);
        }
        if (num2 == 0u)
        {
            return string.Format("{0:000}", num3);
        }
        return string.Format("{0:000} {1:000}", num2, num3);
    }

    void Awake()
    {
       
        lastInterval = Time.realtimeSinceStartup;
        frames = 0;
    }

    private void Update()
    {
        ++frames;
        float timeNow = Time.realtimeSinceStartup;
        if (timeNow > lastInterval + updateInterval)
        {
            fps = (float)(frames / (timeNow - lastInterval));
            frames = 0;
            lastInterval = timeNow;
        }

        pingText.text = "PING: " + Game.Connection.Ping + "ms";
        fpsText.text = "FPS: " + fps.ToString("f2");
    }
}
  

