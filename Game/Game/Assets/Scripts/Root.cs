﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class Root : MonoBehaviour
{
    public SpaceObject spaceObject;

    private List<Spot> spots;
    private List<Weapon> weapons;

    public List<GameObject> slots;
    public GameObject fxGun;

    public Vector3 Position
    {
        get
        {
            return transform.position;
        }
    }

    public Root ()
    {
        spots = new List<Spot>();
        weapons = new List<Weapon>();
    }

    public float delay = 0.5f;
    public float nextFire;

    public float angle = 45.0f;


    public bool isDebugFire = false;

    private void Start()
    {
        //CreateSpot(0, new Vector3(-0.5f, 0f, -0.5f), Quaternion.Euler(0f, -90f, 0f));
        //CreateSpot(1, new Vector3(-0.5f, 0f, -0.25f), Quaternion.Euler(0f, -90f, 0f));
        //CreateSpot(2, new Vector3(-0.5f, 0f, 0f), Quaternion.Euler(0f, -90f, 0f));
        //CreateSpot(3, new Vector3(-0.5f, 0f, 0.25f), Quaternion.Euler(0f, -90f, 0f));

        for (int i = 0; i < slots.Count; i++)
        {
            CreateSpot(i, slots[i].gameObject.transform.position, slots[i].gameObject.transform.rotation);
            InitWeapon(i, new Gun(delay, this));
        }

        //InitWeapon(0, new Gun());
        //InitWeapon(1, new Gun());
        //InitWeapon(2, new Gun());
        //InitWeapon(3, new Gun());
    }

    private void LateUpdate()
    {
        if (Time.time > nextFire + 1f/ delay && (Input.GetKey(KeyCode.G) || isDebugFire))
        {
            nextFire = Time.time;

            foreach (Weapon weapon in weapons)
            {
                Spot spot = spots[weapon.SpotId];

                //if (Angle(spot.transform.forward, target.transform.position - spot.transform.position) <= angle )
                //{
                //    weapon.Shot(target);
                //    Debug.Log("Strzela:" + weapon.SpotId);
                //}

                if (Vector3.Angle(spot.transform.forward, target.transform.position - spot.transform.position) <= angle)
                {
                    weapon.Shot(target);
                    Debug.Log("Strzela:" + weapon.SpotId);
                }
            }
        }
    }


    public Transform target;

    public void InitWeapon(int spotId, Weapon weapon)
    {
        Weapon foundWeapon = weapons.Find(weapon2 => weapon2.SpotId == spotId);

        if(foundWeapon != null)
        {
            return;
        }

        Spot spot = GetSpot(spotId);

        if(spot != null)
        {
            weapon.SetSpot(spot);
            weapons.Add(weapon);
        }
    }

    public Spot GetSpot(int id)
    {
        return spots.Find(spot => spot.Id == id);
    }

    public void CreateSpot(int id, Vector3 localPosition, Quaternion localRotation)
    {
        Spot foundSpot = spots.Find(spot => spot.Id == id);
        
        if(foundSpot != null)
        {
            return;
        }

        Spot newSpot = new Spot(id, localPosition, localRotation);
        newSpot.Initialize(this);

        spots.Add(newSpot);
    }


    private void OnGUI()
    {
        StringBuilder t = new StringBuilder();
        foreach (Weapon weapon in weapons)
        {
            Spot spot = spots[weapon.SpotId];

            bool canShot = Vector3.Angle(spot.transform.forward, target.transform.position - spot.transform.position) <= angle;
            t.Append("Gun: " + spot.Id + " Angle: " + Vector3.Angle(spot.transform.forward, target.transform.position - spot.transform.position) + " can shot: " + canShot + " \n");
        }
        GUILayout.Box(t.ToString());

        if (GUILayout.Button("Shot"))
        {
            foreach (Weapon weapon in weapons)
            {
                Spot spot = spots[weapon.SpotId];

                if (Angle(spot.transform.forward, target.transform.position - spot.transform.position) <=angle)
                {
                    weapon.Shot(target);
                    Debug.Log("Strzela:" + weapon.SpotId);
                }
            }
        }

        GUILayout.Box("info: " + Angle(transform.forward, target.transform.position - transform.position).ToString());
    }

    public float Angle(Vector2 a, Vector2 b)
    {
        float cos = (a.x * b.x + a.y * b.y) / (Mathf.Sqrt(a.x * a.x + a.y * a.y) * Mathf.Sqrt(b.x * b.x + b.y * b.y));
        float angle = Mathf.Acos(cos) * (180f / Mathf.PI);

        if (cos < 0f)
        {
            angle = 360 - angle;
        }

        return angle;
    }
}
