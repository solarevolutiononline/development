﻿using UnityEngine;

public class Spot
{
    public int Id { get; set; }
    public Vector3 LocalPosition { get; private set; }
    public Quaternion LocalRotation { get; private set; }

    public Transform transform;
    private GameObject gameObject;


    public Vector3 Position
    {
        get
        {
            return transform.position;
        }
    }

    public Quaternion Rotation
    {
        get
        {
            return transform.rotation;
        }
    }


    public Spot(int id, Vector3 localPosition, Quaternion localRotation)
    {
        this.Id = id;
        this.LocalPosition = localPosition;
        this.LocalRotation = localRotation;
    }

    public void Initialize(Root root)
    {
        gameObject = new GameObject("Spot_" + Id);
        transform = gameObject.transform;
        transform.parent = root.transform;
        transform.position = root.Position + LocalPosition;
        transform.rotation = LocalRotation;
    }
}

public class Weapon
{
    public int SpotId { get; private set; }

    protected Spot spot;

    public Weapon ()
    {
       
    }

    public void SetSpot(Spot spot)
    {
        SpotId = spot.Id;
        this.spot = spot;
    }

    public virtual void Shot(Transform transform)
    {

    }
}

public class Gun : Weapon
{
    public float delay;
    public Root r;

    public float speed;

    public Gun (float delay, Root r)
    {
        this.r = r;
        this.delay = delay;
    }

    public override void Shot(Transform transform)
    {
        //Resources.Load("Bullet")
        GameObject bulletObject = GameObject.Instantiate(GameObject.FindObjectOfType<Root>().fxGun, spot.Position, spot.Rotation) as GameObject;
        Bullet bullet = bulletObject.AddComponent<Bullet>();
        bullet.target = transform;
        bullet.startPosition = GameObject.FindObjectOfType<Root>().transform.position + spot.LocalPosition;

            bullet.te = (transform.position - spot.Position).magnitude / (100f * 0.1f);
       // bullet.te = (transform.position - spot.Position).magnitude / 5f;
     //   Debug.Log(bullet.te);
        bullet.on = true;
      //  bullet.Start();
    }
}
