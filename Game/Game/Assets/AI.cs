﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour
{
    public GameObject ex;
    public GameObject missile;
    public Transform target;
    public float delay;
    public float nextFire;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       if(Time.time > nextFire + delay)
        {
            nextFire = Time.time;
            GameObject missileObject = GameObject.Instantiate(missile, transform) as GameObject;
            Missile m = missileObject.AddComponent<Missile>();
            m.ExplosionPrefab = ex;
            m.Target = target;
            m.Speed = speed;
            m.flag = true;
        }
    }
}
