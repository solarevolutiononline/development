﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestExp : MonoBehaviour
{
    void OnGUI()
    {
        if (GUILayout.Button("Destroy"))
        {
            foreach (Transform t in transform)
            {
                t.GetComponent<BoxCollider>().isTrigger = false;
            }
        }

        if (GUILayout.Button("Reset"))
        {
            foreach (Transform t in transform)
            {
                t.transform.position = transform.position;
                t.rotation = transform.rotation;
            }
        }
    } 
}
