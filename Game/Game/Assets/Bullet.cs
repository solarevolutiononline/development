﻿using UnityEngine;

public class Bullet : MonoBehaviour
{

    public Transform target;

   public Vector3 startPosition;

  public Vector3 lastPosition;

    public Vector3 Position
    {
        get
        {
            return transform.position;
        }
        set
        {
            transform.position = value;
        }
    }

    public Quaternion Rotation
    {
        get
        {
            return transform.rotation;
        }
        set
        {
            transform.rotation = value;
        }
    }

    public bool on;
    public Bullet()
    {

    }


    public void Start()
    {
        startPosition = transform.position;
    }

    public float delta;
    public float te;

    public float speed;

    private void LateUpdate()
    {
        if (on)
        {
            lastPosition = transform.position;
            transform.position = Vector3.Lerp(startPosition, target.position, delta);

            delta += 1f / te * Time.deltaTime;

            Vector3 forward = transform.position - lastPosition;

            // transform.rotation = Quaternion.LookRotation(target.position);
            transform.LookAt(target);
            if (forward.sqrMagnitude > 0.001f)
            {
              //  transform.rotation = Quaternion.LookRotation(forward);
            }

            if (delta > 1)
            {
                Destroy(gameObject);
            }
        }
    }
}


